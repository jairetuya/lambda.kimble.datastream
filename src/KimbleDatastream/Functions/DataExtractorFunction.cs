using Amazon.Lambda.Core;
using KimbleDatastream.Application.Items.Commands.Requests;
using KimbleDatastream.Application.Items.Queries.Requests;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace KimbleDatastream.Functions
{
    public class DataExtractorFunction
    {
        private readonly IServiceProvider _serviceProvider;

        #region Constructor

        public DataExtractorFunction() : this(Startup.BuildContainer().BuildServiceProvider())
        {
        }

        public DataExtractorFunction(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        #endregion Constructor

        [LambdaSerializer(typeof(Amazon.Lambda.Serialization.SystemTextJson.DefaultLambdaJsonSerializer))]
        public async Task<string> Run(ILambdaContext context)
        {
            context.Logger.LogLine($"Kimble data extractor invoked: {DateTime.Now}");

            //init mediator service provider
            var mediator = _serviceProvider.GetService<IMediator>();

            //get schema content
            (var schemaList, var configDatastamp) = await mediator.Send(new GetSchemaQuery());

            //set generated files list container
            var generatedFiles = new List<string>();

            if (schemaList.Count > 0)
            {
                //upload template data
                generatedFiles = await mediator.Send(new UploadTemplateDataCommand
                {
                    SchemaList = schemaList,
                    ConfigDatastamp = configDatastamp
                });
            }

            var runMessage = "The Kimble data extractor run successfully.";
            if (generatedFiles.Count == 0)
            {
                runMessage += " No files uploaded.";
            }
            else
            {
                runMessage += $" The following ({generatedFiles.Count}) files are uploaded: {string.Join(", ", generatedFiles)}";
            }

            context.Logger.LogLine(runMessage);

            return runMessage;
        }
    }
}