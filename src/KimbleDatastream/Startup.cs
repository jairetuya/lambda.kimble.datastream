﻿using KimbleDatastream.Application;
using KimbleDatastream.Application.Configs;
using KimbleDatastream.Application.Constants;
using KimbleDatastream.Extensions;
using KimbleDatastream.Infrastructure;
using KimbleDatastream.Infrastructure.Configs;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.IO;

namespace KimbleDatastream
{
    public class Startup
    {
        public static IServiceCollection BuildContainer()
        {
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddEnvironmentVariables()
                .Build();

            return ConfigureServices(configuration);
        }

        private static IServiceCollection ConfigureServices(IConfigurationRoot configurationRoot)
        {
            var services = new ServiceCollection();

            services
                .AddApplication()
                .AddInfrastructure()
                .BindAndConfigure(configurationRoot.GetSection(nameof(ReportConfigs)), new ReportConfigs())
                .BindAndConfigure(configurationRoot.GetSection(nameof(Constants)), new Constants())
                .BindAndConfigure(configurationRoot.GetSection(nameof(AwsConfig)), new AwsConfig())
                .BindAndConfigure(configurationRoot.GetSection(nameof(KimbleConfig)), new KimbleConfig());

            return services;
        }
    }
}