﻿using Newtonsoft.Json.Linq;

namespace KimbleDatastream.Application.Models
{
    public class DataRequestModel
    {
        public string SchemaName { get; set; }

        public JObject SchemaTemplate { get; set; }

        public string SchemaDatastamp { get; set; }

        public double TotalRunTime { get; set; }
    }
}