﻿using System.Data;

namespace KimbleDatastream.Application.Models
{
    public class DataResponseModel
    {
        public DataTable Data { get; set; }

        public bool RunOutTime { get; set; } = false;

        public double Runtime { get; set; }

        public bool HasErrors { get; set; } = false;
    }
}