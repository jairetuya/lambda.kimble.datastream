﻿namespace KimbleDatastream.Application.Models
{
    public class BuildReportDataResponseModel
    {
        public bool HasData { get; set; }

        public bool RunOutTime { get; set; } = false;

        public double RunTime { get; set; }

        public string LastRecordDatastamp { get; set; }

        public bool LastRunHasErrors { get; set; } = false;
    }
}