﻿using System.Text.Json.Serialization;

namespace KimbleDatastream.Application.Models
{
    public class ResponseItemModel
    {
        [JsonPropertyName("totalSize")]
        public int TotalSize { get; set; }

        [JsonPropertyName("done")]
        public bool Done { get; set; }

        [JsonPropertyName("nextRecordsUrl")]
        public string NextRecordsUrl { get; set; }
    }
}