﻿using MediatR;
using System.Collections.Generic;

namespace KimbleDatastream.Application.Items.Queries.Requests
{
    public class GetSchemaQuery : IRequest<(List<string>, string)>
    {
    }
}