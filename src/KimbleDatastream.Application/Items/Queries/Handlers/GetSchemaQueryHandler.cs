﻿using KimbleDatastream.Application.Interfaces;
using KimbleDatastream.Application.Items.Queries.Requests;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace KimbleDatastream.Application.Items.Queries.Handlers
{
    public class GetSchemaQueryHandler : IRequestHandler<GetSchemaQuery, (List<string>, string)>
    {
        private readonly IReportFileService _reportFileService;

        public GetSchemaQueryHandler(IReportFileService reportFileService)
        {
            _reportFileService = reportFileService;
        }

        public async Task<(List<string>, string)> Handle(GetSchemaQuery request, CancellationToken cancellationToken)
        {
            return await _reportFileService.GetSchemaList();
        }
    }
}