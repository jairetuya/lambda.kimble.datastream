﻿using KimbleDatastream.Application.Configs;
using KimbleDatastream.Application.Enums;
using KimbleDatastream.Application.Interfaces;
using KimbleDatastream.Application.Items.Commands.Requests;
using MediatR;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace KimbleDatastream.Application.Items.Commands.Handlers
{
    public class UploadTemplateDataCommandHandler : IRequestHandler<UploadTemplateDataCommand, List<string>>
    {
        private readonly ICsvFileService _csvFileService;
        private readonly IParquetFileService _parquetFileService;
        private readonly ReportConfigs _reportConfigs;

        public UploadTemplateDataCommandHandler
        (
            IParquetFileService parquetFileService,
            ICsvFileService csvFileService,
            ReportConfigs reportConfigs
        )
        {
            _parquetFileService = parquetFileService;
            _csvFileService = csvFileService;
            _reportConfigs = reportConfigs;
        }

        public async Task<List<string>> Handle(UploadTemplateDataCommand command, CancellationToken cancellationToken)
        {
            var uploadedFiles = new List<string>();

            if (string.Equals(_reportConfigs.FileType, nameof(FileType.CSV), StringComparison.OrdinalIgnoreCase))
            {
                uploadedFiles = await _csvFileService.UploadData(command, cancellationToken);
            }
            else if (string.Equals(_reportConfigs.FileType, nameof(FileType.Parquet), StringComparison.OrdinalIgnoreCase))
            {
                uploadedFiles = await _parquetFileService.UploadData(command, cancellationToken);
            }

            return uploadedFiles;
        }
    }
}