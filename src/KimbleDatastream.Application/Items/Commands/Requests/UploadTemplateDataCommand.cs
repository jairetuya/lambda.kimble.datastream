﻿using MediatR;
using System.Collections.Generic;

namespace KimbleDatastream.Application.Items.Commands.Requests
{
    public class UploadTemplateDataCommand : IRequest<List<string>>
    {
        public List<string> SchemaList { get; set; }

        public string ConfigDatastamp { get; set; }
    }
}