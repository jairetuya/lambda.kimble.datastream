﻿namespace KimbleDatastream.Application.Enums
{
    public enum FileType
    {
        Parquet,
        CSV
    }
}