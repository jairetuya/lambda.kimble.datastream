﻿namespace KimbleDatastream.Application.Enums
{
    public enum AuthenticationMode
    {
        PWD,
        JWT
    }
}