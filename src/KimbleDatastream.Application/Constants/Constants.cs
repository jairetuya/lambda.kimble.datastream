﻿namespace KimbleDatastream.Application.Constants
{
    public class Constants
    {
        //set run-time limit; lambda runtime-limit is 15 mins; allowing 30 seconds for file bucket upload
        public double RunTimeLimit = 14.0 * 60 * 1000;
    }
}