﻿using KimbleDatastream.Application.Models;
using System.Threading;
using System.Threading.Tasks;

namespace KimbleDatastream.Application.Interfaces
{
    public interface IKimbleService
    {
        Task<DataResponseModel> GetData(DataRequestModel request, CancellationToken cancellationToken);
    }
}