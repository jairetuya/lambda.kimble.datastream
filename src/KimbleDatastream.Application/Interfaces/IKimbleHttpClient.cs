﻿using System.Threading;
using System.Threading.Tasks;

namespace KimbleDatastream.Application.Interfaces
{
    public interface IKimbleHttpClient
    {
        Task<string> GetResponseString(string uri, CancellationToken cancellationToken);
    }
}