﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace KimbleDatastream.Application.Interfaces
{
    public interface IReportFileService
    {
        Task<(List<string>, string)> GetSchemaList();

        Task<string> GetSecretAccessKey();

        Task<bool> UploadFile(MemoryStream memoryStream, string folderName);

        Task<string> UpdateSchemaDatastamp(string schemaName, string schemaDatastamp);
    }
}