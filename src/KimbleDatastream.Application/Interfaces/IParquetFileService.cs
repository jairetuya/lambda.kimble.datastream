﻿using KimbleDatastream.Application.Items.Commands.Requests;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace KimbleDatastream.Application.Interfaces
{
    public interface IParquetFileService
    {
        Task<List<string>> UploadData(UploadTemplateDataCommand data, CancellationToken cancellationToken);
    }
}