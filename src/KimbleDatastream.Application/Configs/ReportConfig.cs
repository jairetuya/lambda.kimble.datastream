﻿namespace KimbleDatastream.Application.Configs
{
    public class ReportConfigs
    {
        public string FileType { get; set; }

        public string BucketName { get; set; }

        public string SchemaFiles { get; set; }

        public string ParameterStoreName { get; set; }

        public string SchemaConfigParameterStoreName { get; set; }

        public int ResetHour { get; set; } = 1;
    }
}