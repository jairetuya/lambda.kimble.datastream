﻿using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using Amazon.SimpleSystemsManagement;
using Amazon.SimpleSystemsManagement.Model;
using KimbleDatastream.Application.Configs;
using KimbleDatastream.Application.Interfaces;
using KimbleDatastream.Infrastructure.Configs;
using KimbleDatastream.Infrastructure.Factories;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace KimbleDatastream.Infrastructure.Storage
{
    public class S3ReportFileService : IReportFileService
    {
        #region Private Fields

        private readonly AmazonS3Client _s3Client;
        private readonly AmazonSimpleSystemsManagementClient _ssmClient;
        private readonly ReportConfigs _reportConfigs;
        private readonly AwsConfig _awsConfig;
        private readonly string _schemaParameterStoreName;

        #endregion Private Fields

        #region Constructor

        public S3ReportFileService
        (
            IAwsClientFactory<AmazonS3Client> clientFactory,
            IAwsClientFactory<AmazonSimpleSystemsManagementClient> ssmClientFactory,
            ReportConfigs reportConfigs,
            AwsConfig awsConfig
        )
        {
            _s3Client = clientFactory.GetAwsClient();
            _ssmClient = ssmClientFactory.GetAwsClient();
            _reportConfigs = reportConfigs;
            _awsConfig = awsConfig;

            _schemaParameterStoreName = _reportConfigs.SchemaConfigParameterStoreName;
        }

        #endregion Constructor

        public async Task<(List<string>, string)> GetSchemaList()
        {
            var schemaList = new List<string>();

            try
            {
                //set bucket request info
                var request = new ListObjectsV2Request
                {
                    BucketName = _reportConfigs.BucketName,
                    Prefix = _reportConfigs.SchemaFiles
                };

                ListObjectsV2Response response;

                do
                {
                    response = await _s3Client.ListObjectsV2Async(request);

                    //loop each S3 objects; read objects inside the SchemaFiles folder
                    foreach (S3Object entry in response.S3Objects)
                    {
                        if (entry.Key.Contains(_reportConfigs.SchemaFiles) && entry.Key != $"{_reportConfigs.SchemaFiles}/")
                        {
                            var objResponse = await _s3Client.GetObjectAsync(new GetObjectRequest
                            {
                                BucketName = _reportConfigs.BucketName,
                                Key = entry.Key,
                            });

                            var schemaContent = new StreamReader(objResponse?.ResponseStream)?.ReadToEnd();

                            if (!string.IsNullOrEmpty(schemaContent))
                            {
                                if (IsValidSchema(schemaContent))
                                {
                                    schemaList.Add(schemaContent);
                                }
                            }
                        }
                    }

                    request.ContinuationToken = response.NextContinuationToken;
                } while (response.IsTruncated);
            }
            catch (AmazonS3Exception s3Ex)
            {
                Console.WriteLine("S3 error occurred. Exception: " + s3Ex.ToString());
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error retrieving schema files to S3 bucket. Error: {0}", ex.Message);
            }

            //manage schema configuration tracker
            var configDatastamp = await SetSchemaConfigurations(schemaList);

            return (schemaList, configDatastamp);
        }

        public async Task<bool> UploadFile(MemoryStream memoryStream, string keyName)
        {
            try
            {
                var transferUtility = new TransferUtility(_s3Client);
                await transferUtility.UploadAsync(memoryStream, _reportConfigs.BucketName, keyName);

                return true;
            }
            catch (Exception ex)
            {
                throw new Exception($"Error uploading stream file ({keyName}) to S3 bucket. Error: {ex.Message}");
            }
        }

        public async Task<string> GetSecretAccessKey()
        {
            try
            {
                //set parameter request/response
                var response = await _ssmClient.GetParameterAsync(new GetParameterRequest()
                {
                    Name = _reportConfigs.ParameterStoreName,
                    WithDecryption = true
                });

                //return parameter value
                return response?.Parameter?.Value;
            }
            catch (Exception ex)
            {
                throw new Exception($"Unable to get secret access key. Error: {ex.Message}");
            }
        }

        public async Task<string> UpdateSchemaDatastamp(string schemaName, string schemaDatastamp)
        {
            try
            {
                var updateStatus = "Datastamp is empty.";

                if (!string.IsNullOrWhiteSpace(schemaDatastamp))
                {
                    //retrieve config datastamp
                    var currentSchemaConfigDatstamp = await _ssmClient.GetParameterAsync(new GetParameterRequest()
                    {
                        Name = _schemaParameterStoreName,
                        WithDecryption = true
                    });

                    //return parameter value
                    var schemaConfigDataStamp = currentSchemaConfigDatstamp?.Parameter?.Value;

                    //set JObject template
                    JObject configDataStampTemplate = JObject.Parse(schemaConfigDataStamp);

                    //get schemaList
                    var schemaList = new List<string>();
                    var schemas = $"{configDataStampTemplate["Schemas"]}";
                    if (!string.IsNullOrWhiteSpace(schemas))
                    {
                        var arrSchema = schemas.Split(",");
                        schemaList = new List<string>(arrSchema);
                    }

                    //compose schema config value
                    var updatedSchemaDatastamp = new StringBuilder();
                    updatedSchemaDatastamp.Append("{");
                    updatedSchemaDatastamp.Append($"\"Schemas\":\"{string.Join(",", schemaList)}\",");

                    foreach (var s in schemaList)
                    {
                        var datastamp = s == schemaName ? schemaDatastamp : $"{configDataStampTemplate[s]}";
                        updatedSchemaDatastamp.Append($"\"{s}\":\"{datastamp}\",");
                    }

                    updatedSchemaDatastamp.Append("}");

                    //-------------------------------------------------------------------------
                    try
                    {
                        //update schema config datastamp
                        var response = await _ssmClient.PutParameterAsync(new PutParameterRequest()
                        {
                            Name = _schemaParameterStoreName,
                            Value = updatedSchemaDatastamp.ToString(),
                            Overwrite = true
                        });

                        updateStatus = response?.HttpStatusCode.ToString();
                    }
                    catch (Exception ex)
                    {
                        //create parameter store and save the schema config
                        await CreateParameterStore(updatedSchemaDatastamp.ToString());

                        Console.WriteLine($"Unable to update schema configuration parameter store ({_schemaParameterStoreName}). Error: {ex.Message}");
                    }
                }

                return updateStatus;
            }
            catch (Exception ex)
            {
                var excMsg = $"Unable to update schema config datastamp in parameter store ({_schemaParameterStoreName}). Error: {ex.Message}";
                Console.WriteLine(excMsg);
                return excMsg;
            }
        }

        #region Private Methods

        private static bool IsValidSchema(string schema)
        {
            //set JObject template
            JObject template = JObject.Parse(schema);

            //check required template fields; skip if not found
            if (template.ContainsKey("DataLakeName") && template.ContainsKey("CallType"))
            {
                switch ($"{template["CallType"]}")
                {
                    case "Endpoint":
                        if (!template.ContainsKey("Endpoint"))
                        {
                            return false;
                        }

                        var endpoint = $"{template["Endpoint"]}";
                        if (string.IsNullOrEmpty(endpoint))
                        {
                            return false;
                        }

                        break;

                    case "Query":
                        if (!template.ContainsKey("Object") || !template.ContainsKey("Fields"))
                        {
                            return false;
                        }

                        var sobject = $"{template["Object"]}";
                        var fields = $"{template["Fields"]}";
                        if (string.IsNullOrEmpty(sobject) || string.IsNullOrEmpty(fields))
                        {
                            return false;
                        }

                        break;
                }

                return true;
            }

            return false;
        }

        private async Task<string> SetSchemaConfigurations(List<string> schemaList)
        {
            //set schemas container
            var schemas = new List<string>();

            //loop through schema list
            foreach (var strSchema in schemaList)
            {
                //set JObject template
                JObject schemaTemplate = JObject.Parse(strSchema);

                //get names
                var dataLakeName = $"{schemaTemplate["DataLakeName"]}";
                dataLakeName = dataLakeName.Trim().Replace(" ", "");

                var dataFileName = $"{schemaTemplate["DataFileName"]}";
                dataFileName = dataFileName.Trim().Replace(" ", "");

                //set identifier
                var schemaName = $"{dataLakeName}/{dataFileName}";

                if (schemas.IndexOf(schemaName) < 0) //this is ensure no similar datalakename is set in a separate schema file
                {
                    schemas.Add(schemaName);
                }
            }

            //compose schema config value
            var schemaConfigs = new StringBuilder();
            schemaConfigs.Append("{");
            schemaConfigs.Append($"\"Schemas\":\"{string.Join(",", schemas)}\",");

            foreach (var s in schemas)
            {
                schemaConfigs.Append($"\"{s}\":\"#NotStarted\",");
            }

            schemaConfigs.Append("}");

            //init config datastamp value
            var configDatastamp = schemaConfigs.ToString();

            try
            {
                //update (create) the config datastamp
                if (DateTime.Now.Hour == _reportConfigs.ResetHour) // will only configure on the reset-hour hourly-run
                {
                    var response = await _ssmClient.PutParameterAsync(new PutParameterRequest()
                    {
                        Name = _schemaParameterStoreName,
                        Value = schemaConfigs.ToString(),
                        Overwrite = true
                    });
                }
                else
                {
                    //get schema config datastamp
                    var response = await _ssmClient.GetParameterAsync(new GetParameterRequest()
                    {
                        Name = _schemaParameterStoreName,
                        WithDecryption = true
                    });

                    //return parameter value
                    configDatastamp = response?.Parameter?.Value ?? schemaConfigs.ToString();
                }
            }
            catch (Exception ex)
            {
                //create parameter store and save the schema config
                await CreateParameterStore(schemaConfigs.ToString());

                Console.WriteLine($"Unable to update schema configuration parameter store ({_schemaParameterStoreName}). Error: {ex.Message}");
            }

            return configDatastamp;
        }

        private async Task CreateParameterStore(string paramStoreValue)
        {
            await _ssmClient.PutParameterAsync(new PutParameterRequest()
            {
                Name = _schemaParameterStoreName,
                Value = paramStoreValue,
                Overwrite = true,
                Tier = ParameterTier.Standard,
                Type = ParameterType.String
            });

            Console.WriteLine($"Kimble schema configuration parameter store ({_schemaParameterStoreName}) is successfully created.");
        }

        #endregion Private Methods
    }
}