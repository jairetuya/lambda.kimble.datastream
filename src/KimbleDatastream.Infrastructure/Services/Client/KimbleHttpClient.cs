﻿using JWT;
using JWT.Algorithms;
using JWT.Serializers;
using KimbleDatastream.Application.Enums;
using KimbleDatastream.Application.Interfaces;
using KimbleDatastream.Infrastructure.Configs;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.OpenSsl;
using Org.BouncyCastle.Security;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace KimbleDatastream.Infrastructure.Services.Client
{
    public class KimbleHttpClient : IKimbleHttpClient
    {
        #region Private Fields

        private readonly KimbleConfig _kimbleConfig;
        private readonly HttpClient _apiClient;
        private readonly IReportFileService _reportFileService;
        private readonly Stopwatch _timer;
        private KimbleToken _kimbleToken;

        #endregion Private Fields

        #region Constructor

        public KimbleHttpClient(HttpClient apiClient, KimbleConfig kimbleConfig, IReportFileService reportFileService)
        {
            _apiClient = apiClient;
            _kimbleConfig = kimbleConfig;
            _kimbleToken = new KimbleToken();
            _reportFileService = reportFileService;
            _timer = new Stopwatch();
        }

        #endregion Constructor

        public async Task<string> GetResponseString(string uri, CancellationToken cancellationToken)
        {
            try
            {
                //get kimble token
                var kimbleToken = await GetKimbleToken(_kimbleConfig.AuthenticationMode, cancellationToken);

                if (!string.IsNullOrEmpty(kimbleToken.AccessToken) && !string.IsNullOrEmpty(kimbleToken.InstanceUrl))
                {
                    //set instance Url without the /
                    var instanceUrl = (kimbleToken.InstanceUrl.EndsWith("/")) ? kimbleToken.InstanceUrl.Remove(kimbleToken.InstanceUrl.Length - 1, 1) : kimbleToken.InstanceUrl;

                    //complete Uri by prepending the instance URL
                    uri = $"{instanceUrl}/{uri}";

                    using (var request = new HttpRequestMessage(HttpMethod.Get, uri))
                    {
                        request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", kimbleToken.AccessToken);

                        Console.Write("Sending request to: {0} ...", uri);
                        _timer.Start();

                        var responseMessage = await _apiClient.SendAsync(request, cancellationToken);

                        _timer.Stop();
                        Console.WriteLine(" received data after {0}ms", _timer.ElapsedMilliseconds);

                        if (responseMessage.IsSuccessStatusCode)
                        {
                            return await responseMessage.Content.ReadAsStringAsync();
                        }
                        else
                        {
                            Console.WriteLine("Receive unsuccessful response from {0}. Status code {1}", request.RequestUri.AbsolutePath, responseMessage.StatusCode);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Unable to get Kimble data. Request: {uri} Error: {ex.Message}");
            }

            return string.Empty;
        }

        #region Private Methods

        private async Task<KimbleToken> GetKimbleToken(string authMode, CancellationToken cancellationToken)
        {
            try
            {
                if (_kimbleToken != null && !string.IsNullOrEmpty(_kimbleToken.AccessToken) && !string.IsNullOrEmpty(_kimbleToken.InstanceUrl))
                {
                    return _kimbleToken;
                }
                else
                {
                    Console.Write("Requesting Kimble token... ");
                    _timer.Start();

                    //set Uri parameter
                    var data = new Dictionary<string, string>();
                    var tokenUri = $"{_kimbleConfig.ServiceUri}/{_kimbleConfig.TokenEndpoint}";

                    switch ((AuthenticationMode)Enum.Parse(typeof(AuthenticationMode), authMode))
                    {
                        case AuthenticationMode.PWD:
                            data = new Dictionary<string, string>
                            {
                                { "grant_type", "password" },
                                { "username", _kimbleConfig.Username },
                                { "password", _kimbleConfig.Password },
                                { "client_id", _kimbleConfig.ClientId },
                                { "client_secret", _kimbleConfig.ClientSecret },
                            };

                            break;

                        case AuthenticationMode.JWT:
                            data = new Dictionary<string, string>
                            {
                                { "grant_type", "urn:ietf:params:oauth:grant-type:jwt-bearer" },
                                { "assertion", await GenerateJWT() }
                            };

                            break;
                    }

                    using (var tokenRequest = new HttpRequestMessage(HttpMethod.Post, tokenUri))
                    {
                        tokenRequest.Content = new FormUrlEncodedContent(data);

                        //send request
                        var httpResponseMessage = await _apiClient.SendAsync(tokenRequest, cancellationToken);

                        //check status code
                        if (httpResponseMessage.IsSuccessStatusCode)
                        {
                            var responseString = await httpResponseMessage.Content.ReadAsStringAsync();

                            _timer.Stop();
                            Console.WriteLine($"received after {_timer.ElapsedMilliseconds}ms");

                            _kimbleToken = JsonSerializer.Deserialize<KimbleToken>(responseString);

                            return _kimbleToken;
                        }
                        else
                        {
                            throw new Exception($"Unable to authenticate against Kimble. Status code {httpResponseMessage.StatusCode}");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"Unable to get Kimble Token. Error: {ex.Message}");
            }
        }

        private async Task<string> GenerateJSONWebToken()
        {
            try
            {
                //retrieve private key
                Console.Write("Getting Kimble secret access key... ");
                string accessKey = await _reportFileService.GetSecretAccessKey();

                if (!string.IsNullOrEmpty(accessKey))
                {
                    Console.Write("Kimble secret access key retrieved.");
                    Console.Write($"Secret Key: {accessKey}");

                    RSAParameters rsaParams;
                    using (var tr = new StringReader(accessKey))
                    {
                        var pemReader = new PemReader(tr);
                        if (!(pemReader.ReadObject() is AsymmetricCipherKeyPair keyPair))
                        {
                            throw new Exception("Could not read RSA access key.");
                        }

                        var privateRsaParams = keyPair.Private as RsaPrivateCrtKeyParameters;
                        rsaParams = DotNetUtilities.ToRSAParameters(privateRsaParams);

                        Console.Write("Secret access key parameters accepted.");
                    }

                    Console.Write("Composing JWT claims... ");
                    var claims = new[] {
                        new Claim(JwtRegisteredClaimNames.Iss, _kimbleConfig.IssuerKey),
                        new Claim(JwtRegisteredClaimNames.Sub, _kimbleConfig.Username),
                        new Claim(JwtRegisteredClaimNames.Aud, _kimbleConfig.ServiceUri),
                        new Claim(JwtRegisteredClaimNames.Exp, DateTimeOffset.UtcNow.AddSeconds(300).ToUnixTimeSeconds().ToString())
                    };

                    using (RSACryptoServiceProvider rsa = new RSACryptoServiceProvider())
                    {
                        rsa.ImportParameters(rsaParams);
                        Console.Write("Secret access key parameters imported.");

                        Dictionary<string, object> payload = claims.ToDictionary(k => k.Type, v => (object)v.Value);
                        Console.Write("Secret access key claims added to payload.");

                        return Jose.JWT.Encode(payload, rsa, Jose.JwsAlgorithm.RS256);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"Unable to generate JWT. Error: {ex.Message}");
            }

            return string.Empty;
        }

        private async Task<string> GenerateJWT()
        {
            try
            {
                //retrieve private key
                Console.Write("Getting Kimble secret access key... ");
                string accessKey = await _reportFileService.GetSecretAccessKey();

                if (!string.IsNullOrEmpty(accessKey))
                {
                    Console.Write("Kimble secret access key retrieved.");

                    var rsaParams = GetRsaParameters(accessKey);
                    var encoder = GetRS256JWTEncoder(rsaParams);

                    // create the payload according to your need
                    var payload = new Dictionary<string, object>
                    {
                        { "iss", _kimbleConfig.IssuerKey},
                        { "sub", _kimbleConfig.Username },
                        { "aud", _kimbleConfig.ServiceUri },
                        { "exp", DateTimeOffset.UtcNow.AddSeconds(300).ToUnixTimeSeconds().ToString() }
                    };

                    // add headers. 'alg' and 'typ' key-values are added automatically.
                    var header = new Dictionary<string, object>();

                    var token = encoder.Encode(header, payload, new byte[0]);

                    return token;
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"Unable to generate JWT. Error: {ex.Message}");
            }

            return string.Empty;
        }

        private static IJwtEncoder GetRS256JWTEncoder(RSAParameters rsaParams)
        {
            try
            {
                var csp = new RSACryptoServiceProvider();
                csp.ImportParameters(rsaParams);

                var algorithm = new RS256Algorithm(csp, csp);
                var serializer = new JsonNetSerializer();
                var urlEncoder = new JwtBase64UrlEncoder();
                var encoder = new JwtEncoder(algorithm, serializer, urlEncoder);

                return encoder;
            }
            catch (Exception ex)
            {
                throw new Exception($"Unable to get RS256 JWTEncoder. Error: {ex.Message}");
            }
        }

        private static RSAParameters GetRsaParameters(string rsaPrivateKey)
        {
            try
            {
                var byteArray = Encoding.ASCII.GetBytes(rsaPrivateKey);
                using (var ms = new MemoryStream(byteArray))
                {
                    using (var sr = new StreamReader(ms))
                    {
                        // use Bouncy Castle to convert the private key to RSA parameters
                        var pemReader = new PemReader(sr);
                        var keyPair = pemReader.ReadObject() as AsymmetricCipherKeyPair;
                        return DotNetUtilities.ToRSAParameters(keyPair.Private as RsaPrivateCrtKeyParameters);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"Unable to get RSA parameters. Error: {ex.Message}");
            }
        }

        #endregion Private Methods
    }
}