﻿using KimbleDatastream.Application.Constants;
using KimbleDatastream.Application.Interfaces;
using KimbleDatastream.Application.Models;
using KimbleDatastream.Infrastructure.Configs;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace KimbleDatastream.Infrastructure.Services.Client
{
    public class KimbleService : IKimbleService
    {
        #region Private Fields

        private readonly IKimbleHttpClient _kimbleHttpClient;
        private readonly KimbleConfig _kimbleConfig;
        private readonly Constants _constants;
        private readonly Stopwatch _timer;

        #endregion Private Fields

        #region Constructor

        public KimbleService(IKimbleHttpClient kimbleHttpClient, KimbleConfig kimbleConfig, Constants constants)
        {
            _kimbleHttpClient = kimbleHttpClient;
            _kimbleConfig = kimbleConfig;
            _constants = constants;
            _timer = new Stopwatch();
        }

        #endregion Constructor



        public async Task<DataResponseModel> GetData(DataRequestModel request, CancellationToken cancellationToken)
        {
            //set run parameters
            var dataArray = new JArray();
            var runOutOfTime = false;
            var currentRunTime = 0.0;
            var totalRuntime = request.TotalRunTime;

            //int data response model
            var dataResponse = new DataResponseModel
            {
                Data = new DataTable(),
                RunOutTime = runOutOfTime,
                Runtime = 0.0
            };

            try
            {
                //compose
                Console.WriteLine($"Composing API data endpoint ... ");
                var dataEndpoint = ComposeApiEndpoint(request.SchemaTemplate);
                Console.WriteLine($"Endpoint Used: {dataEndpoint}");

                if (!string.IsNullOrEmpty(dataEndpoint))
                {
                    //set and init data parameters
                    var dataRecordsEndpoint = dataEndpoint;
                    var dataRetrievalComplete = false;
                    var page = 1;

                    //commence data retrieval until flag set is completed
                    while (!dataRetrievalComplete)
                    {                        
                        //check total running time; break when it's running more than the allowed threshold so as not to lose data
                        if (totalRuntime + currentRunTime >= _constants.RunTimeLimit)
                        {
                            runOutOfTime = true;
                            break;
                        }

                        //start timer
                        var timer = Stopwatch.StartNew();

                        Console.Write($"Requesting page {page} data ... ");

                        //get data string items
                        var stringItems = await _kimbleHttpClient.GetResponseString(dataRecordsEndpoint, cancellationToken);

                        //exit loop if no response string
                        if (string.IsNullOrWhiteSpace(stringItems))
                            break;

                        try
                        {
                            Console.WriteLine($"Deserializing page {page} data... ");

                            using (var jsonDoc = JsonDocument.Parse(stringItems))
                            {
                                var responseItem = JsonSerializer.Deserialize<ResponseItemModel>(stringItems);

                                //----------
                                //> add items
                                //----------
                                try
                                {
                                    //parse data string items
                                    JObject jData = JObject.Parse(stringItems);

                                    //find the first array
                                    var srcArray = jData.Descendants().Where(d => d is JArray).First();

                                    //loop through each descendants
                                    foreach (var row in srcArray.Children<JObject>()) //filter JObject children
                                    {
                                        var cleanRow = new JObject();
                                        foreach (JProperty column in row.Properties())
                                        {
                                            if (column.Value is JValue) //only include JValue types
                                            {
                                                cleanRow.Add(column.Name, column.Value);
                                            }
                                        }

                                        //add to data array
                                        dataArray.Add(cleanRow);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Console.WriteLine("Error encountered while collating records. Data: {0} Error: {1}", stringItems, ex.Message);
                                }

                                //set nextRecordsUrl
                                dataRecordsEndpoint = responseItem.NextRecordsUrl;                                

                                //exit loop if no more records found
                                if (responseItem.Done)
                                {
                                    dataRetrievalComplete = true;                                    
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("Error encountered. Data: {0}. Error: {1}", stringItems, ex.Message);
                        }

                        //update total run time
                        timer.Stop();
                        currentRunTime += timer.ElapsedMilliseconds;

                        //increment page count
                        page++;
                    }
                }
                else
                {
                    Console.WriteLine("Unable to compose the API endpoint from the template.");
                }
            }
            catch (Exception ex)
            {
                dataResponse.HasErrors = true;
                Console.WriteLine("Unable to get Kimble data. Error: {0}", ex.Message);
            }

            //deserialize target data array into a data table
            if (dataArray.Count > 0)
            {
                dataResponse.Data = Newtonsoft.Json.JsonConvert.DeserializeObject<DataTable>(Newtonsoft.Json.JsonConvert.SerializeObject(dataArray));
            }

            //set latest run values
            dataResponse.Runtime = currentRunTime;
            dataResponse.RunOutTime = runOutOfTime;

            return dataResponse;
        }

        #region Private Methods

        private string ComposeApiEndpoint(JObject template)
        {
            var callType = $"{template["CallType"]}";

            switch (callType)
            {
                case "Endpoint":
                    return $"{template["Endpoint"]}";

                case "Query":
                    var apiVersion = $"{template["ApiVersion"]}";
                    var apiQuery = _kimbleConfig.DataEndpoint;

                    if (!string.IsNullOrWhiteSpace(apiVersion))
                    {
                        apiQuery = $"services/data/{apiVersion}/query/?q=";
                    }

                    //init SQL query string
                    var sqlQuery = "SELECT+";

                    //append the data fields
                    var dataFields = template.GetValue("Fields");
                    foreach (string field in dataFields)
                    {
                        sqlQuery += $"{field},";
                    }
                    sqlQuery = sqlQuery[0..^1];

                    //append SObject
                    var sobject = $"{template["Object"]}";
                    sqlQuery += $"+FROM+{sobject}";

                    //append Where if there's any
                    if (template.ContainsKey("Where"))
                    {
                        //get where clause value
                        var whereValues = template.GetValue("Where");

                        if (whereValues.Count() > 0)
                        {
                            //override special operators
                            var allowedSpecialOperators = new List<string> { "in", "not in" };
                            var disallowedSpecialOperators = new List<string> { "starts with", "ends with", "contains" };
                            var invalidSQLKeys = new List<string> { ";", "--", "/*", "*/", "xp_" };
                            var whereClauses = new List<string>();

                            //loop where array values
                            var loopCtr = 0;
                            foreach (var whereInfo in whereValues)
                            {
                                var whereValue = "";
                                var wField = $"{whereInfo[0]}";
                                var wOperator = $"{whereInfo[1]}";
                                var wValue = $"{whereInfo[2]}";
                                var wType = $"{whereInfo[3]}";

                                if (!string.IsNullOrEmpty(wValue) && (invalidSQLKeys.IndexOf(wField) < 0 && invalidSQLKeys.IndexOf(wOperator) < 0))
                                {
                                    wOperator = wOperator.ToLower();
                                    wType = wType.ToLower();

                                    if (allowedSpecialOperators.IndexOf(wOperator) >= 0) //check for allowed special operators
                                    {
                                        switch (wOperator)
                                        {
                                            case "in": whereValue += $"{wField}+IN+({wValue})"; break;
                                            case "not in": whereValue += $"{wField}+NOT+IN+({wValue})"; break;
                                        }
                                    }
                                    else if (disallowedSpecialOperators.IndexOf(wOperator) >= 0) //skip disallowed special operators
                                    {
                                        continue;
                                    }
                                    else
                                    {
                                        switch (wType)
                                        {
                                            case "string": whereValue += $"{wField}+{wOperator}'{wValue}'"; break;

                                            case "date":
                                            case "timestamp":
                                                try
                                                {
                                                    var timestamp = Convert.ToDateTime(wValue);

                                                    whereValue += $"{wField}+{wOperator}+{timestamp:yyyy-MM-ddTHH:mm:ssZ}";
                                                }
                                                catch
                                                {
                                                    continue;
                                                }

                                                break;

                                            default: whereValue += $"{wField}+{wOperator}+{wValue}"; break;
                                        }
                                    }

                                    //add to where array
                                    whereClauses.Add(whereValue);
                                }

                                loopCtr++;
                            }

                            if (whereClauses.Count > 0)
                            {
                                sqlQuery += $"+WHERE+{string.Join("+AND+", whereClauses)}";
                            }
                        }
                    }

                    return apiQuery + sqlQuery;
            }

            return string.Empty;
        }

        #endregion Private Methods
    }
}