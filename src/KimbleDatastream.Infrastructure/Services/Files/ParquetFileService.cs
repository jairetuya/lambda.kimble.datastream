﻿using Humanizer;
using KimbleDatastream.Application.Constants;
using KimbleDatastream.Application.Interfaces;
using KimbleDatastream.Application.Items.Commands.Requests;
using KimbleDatastream.Application.Models;
using Newtonsoft.Json.Linq;
using Parquet;
using Parquet.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace KimbleDatastream.Infrastructure.Services.Files
{
    public class ParquetFileService : IParquetFileService
    {
        private readonly IKimbleService _kimbleService;
        private readonly IReportFileService _reportFileService;
        private readonly Constants _constants;

        public ParquetFileService(IKimbleService kimbleService, IReportFileService reportFileService, Constants constants)
        {
            _kimbleService = kimbleService;
            _reportFileService = reportFileService;
            _constants = constants;
        }

        public async Task<List<string>> UploadData(UploadTemplateDataCommand data, CancellationToken cancellationToken)
        {
            var uploadedFiles = new List<string>();
            var schemaList = new List<string>();
            var configDataStampTemplate = new JObject();
            var completedFlag = "#Completed";
            var totalRunTime = 0.0;

            //get schema config datastamp
            if (!string.IsNullOrWhiteSpace(data.ConfigDatastamp))
            {
                configDataStampTemplate = JObject.Parse(data.ConfigDatastamp);

                //get recordTypes
                var schemas = $"{configDataStampTemplate["Schemas"]}";
                if (!string.IsNullOrWhiteSpace(schemas))
                {
                    var arrSchema = schemas.Split(",");
                    schemaList = new List<string>(arrSchema);
                }
            }

            foreach (var schema in data.SchemaList)
            {
                //check total running time; break when it's running more than the allowed threshold so as not to lose data
                if (totalRunTime >= _constants.RunTimeLimit)
                {
                    break;
                }

                //set JObject template
                JObject schemaTemplate = JObject.Parse(schema);

                //get datalake name
                var dataLakeName = $"{schemaTemplate["DataLakeName"]}";
                dataLakeName = dataLakeName.Trim().Replace(" ", "");

                //get data filename
                var dataFileName = $"{schemaTemplate["DataFileName"]}";
                if (string.IsNullOrWhiteSpace(dataFileName))
                {
                    dataFileName = dataLakeName;
                }
                dataFileName = dataFileName.Trim().Replace(" ", "");

                //set schema-name
                var schemaName = $"{dataLakeName}/{dataFileName}";

                if (schemaList.IndexOf(schemaName) >= 0)
                {
                    //get schema datastamp
                    var schemaDatastamp = $"{configDataStampTemplate[schemaName]}";

                    if (!string.IsNullOrWhiteSpace(schemaDatastamp) && schemaDatastamp != completedFlag)
                    {
                        //generate report file data stream
                        using var dataStream = new MemoryStream();
                        var requestModel = new DataRequestModel
                        {
                            SchemaName = schemaName,
                            SchemaTemplate = schemaTemplate,
                            SchemaDatastamp = schemaDatastamp,
                            TotalRunTime = totalRunTime
                        };

                        //build report data stream
                        var buildDataResponse = await BuildReportDataStream(dataStream, requestModel, cancellationToken);

                        //check run-out-time-stats, break if it's running out time
                        if (buildDataResponse.RunOutTime)
                        {
                            totalRunTime = buildDataResponse.RunTime;
                            break;
                        }

                        //start timer for the other processes as well
                        var timer = Stopwatch.StartNew();

                        //construct key name
                        var keyName = $"{dataLakeName}/kimble_{dataFileName}.parquet";

                        //upload data
                        if (buildDataResponse.HasData)
                        {
                            Console.Write($"Uploading {dataLakeName} file ({keyName}) ...");

                            //upload file to S3 bucket
                            var uploaded = await _reportFileService.UploadFile(dataStream, keyName);

                            if (uploaded)
                            {
                                Console.WriteLine($" file is uploaded successfully.");

                                uploadedFiles.Add(keyName);
                            }
                        }
                        else
                        {
                            uploadedFiles.Add($"NoData_{keyName}");
                        }

                        //update config datastamp
                        var schemaStatusDatastamp = buildDataResponse.RunOutTime || buildDataResponse.LastRunHasErrors ? "#Running" : completedFlag;
                        await _reportFileService.UpdateSchemaDatastamp(schemaName, schemaStatusDatastamp).ConfigureAwait(false);

                        //stop timer
                        timer.Stop();

                        //increment total runtime
                        totalRunTime = buildDataResponse.RunTime + timer.ElapsedMilliseconds;
                    }
                }
            }

            //log total runtime
            var extractionTimeLapse = TimeSpan.FromMilliseconds(totalRunTime).Humanize(4);
            Console.WriteLine("--------------------------------------------------------------------------");
            Console.WriteLine($"Data extraction was completed in - {extractionTimeLapse}.");
            Console.WriteLine("--------------------------------------------------------------------------");

            return uploadedFiles;
        }

        #region Private Methods

        private async Task<BuildReportDataResponseModel> BuildReportDataStream(MemoryStream dataStream, DataRequestModel request, CancellationToken cancellationToken)
        {
            //int build report response
            var buildReportResponse = new BuildReportDataResponseModel
            {
                HasData = false,
                RunOutTime = false,
                RunTime = request.TotalRunTime
            };

            try
            {
                //set current schema config datastamp to #Running
                await _reportFileService.UpdateSchemaDatastamp(request.SchemaName, "#Running").ConfigureAwait(false);

                //get Kimble data
                var kimbleData = await _kimbleService.GetData(request, cancellationToken);

                //set data table
                var dataTable = kimbleData.Data;

                //set response values
                buildReportResponse.RunOutTime = kimbleData.RunOutTime;
                buildReportResponse.RunTime += kimbleData.Runtime;
                buildReportResponse.HasData = dataTable.Columns.Count > 0;
                buildReportResponse.LastRunHasErrors = kimbleData.HasErrors;

                if (dataTable.Rows.Count > 0)
                {
                    //generate field schema
                    var dataSchemaFields = GenerateSchema(dataTable);

                    //set size of a row group
                    var RowGroupSize = dataTable.Rows.Count + 100;

                    using (var writer = new ParquetWriter(new Schema(dataSchemaFields), dataStream))
                    {
                        //init startRow
                        var startRow = 0;

                        //keep on creating row groups until we run out of data
                        while (startRow < dataTable.Rows.Count)
                        {
                            using (var rgw = writer.CreateRowGroup())
                            {
                                // Data is written to the row group column by column
                                for (var i = 0; i < dataTable.Columns.Count; i++)
                                {
                                    //set column index
                                    var colIdx = i;

                                    //determine the target data type for the column
                                    var targetType = dataTable.Columns[colIdx].DataType;

                                    if (targetType == typeof(DateTime) || targetType == typeof(DateTimeOffset))
                                    {
                                        targetType = typeof(long);
                                    }

                                    //generate the value type, this is to ensure it can handle null values
                                    var valueType = targetType.IsClass ? targetType : typeof(Nullable<>).MakeGenericType(targetType);

                                    //create a list to hold values of the required type for the column
                                    var list = (IList)typeof(List<>)
                                                    .MakeGenericType(valueType)
                                                    .GetConstructor(Type.EmptyTypes)
                                                    .Invoke(null);

                                    //get the data to be written to the parquet stream
                                    foreach (var row in dataTable.AsEnumerable().Skip(startRow).Take(RowGroupSize))
                                    {
                                        //check if value is null, if so then add a null value
                                        if (row[colIdx] == null || row[colIdx] == DBNull.Value)
                                        {
                                            list.Add(null);
                                        }
                                        else
                                        {
                                            //init value
                                            var value = row[colIdx];

                                            var dataType = dataTable.Columns[colIdx].DataType;

                                            if (dataType == typeof(string))
                                            {
                                                var stringValue = (string)row[colIdx];

                                                value = stringValue;
                                            }
                                            else if (dataType == typeof(DateTime)) //datetime
                                            {
                                                try
                                                {
                                                    var thisDate = (DateTime)row[colIdx];
                                                    value = ((DateTimeOffset)thisDate).ToUnixTimeMilliseconds();
                                                }
                                                catch
                                                {
                                                    value = null;
                                                }
                                            }
                                            else if (dataType == typeof(System.DateTimeOffset)) //datetimeoffset
                                            {
                                                try
                                                {
                                                    var thisDate = (DateTimeOffset)row[colIdx];
                                                    value = thisDate.ToUnixTimeMilliseconds();
                                                }
                                                catch
                                                {
                                                    value = null;
                                                }
                                            }

                                            //add the value to the list,
                                            list.Add(value);
                                        }
                                    }

                                    //copy the list values to an array of the same type as the WriteColumn method expects
                                    var valuesArray = Array.CreateInstance(valueType, list.Count);
                                    list.CopyTo(valuesArray, 0);

                                    //write the column
                                    rgw.WriteColumn(new Parquet.Data.DataColumn(dataSchemaFields[i], valuesArray));
                                }
                            }

                            //increment startRow
                            startRow += RowGroupSize;
                        }
                    }
                }

                return buildReportResponse;
            }
            catch (Exception ex)
            {
                throw new Exception($"Unable to generate Parquet file. Error Message: {ex.Message}");
            }
        }

        private static List<DataField> GenerateSchema(DataTable dt)
        {
            var fields = new List<DataField>(dt.Columns.Count);

            foreach (System.Data.DataColumn column in dt.Columns)
            {
                // Attempt to parse the type of column to a parquet data type
                var success = Enum.TryParse<DataType>(column.DataType.Name, true, out var type);

                //replace DateTime / DateTimeOffset per requirement
                if (column.DataType == typeof(DateTime) || column.DataType == typeof(DateTimeOffset))
                {
                    type = DataType.Int64;
                }
                else if (!success)
                {
                    type = DataType.String;
                }

                //add column-field
                fields.Add(new DataField(column.ColumnName, type));
            }

            return fields;
        }

        #endregion Private Methods
    }
}