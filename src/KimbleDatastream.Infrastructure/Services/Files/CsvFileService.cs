﻿using Humanizer;
using KimbleDatastream.Application.Constants;
using KimbleDatastream.Application.Interfaces;
using KimbleDatastream.Application.Items.Commands.Requests;
using KimbleDatastream.Application.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace KimbleDatastream.Infrastructure.Services.Files
{
    public class CsvFileService : ICsvFileService
    {
        private readonly IKimbleService _kimbleService;
        private readonly IReportFileService _reportFileService;
        private readonly Constants _constants;

        public CsvFileService(IKimbleService kimbleService, IReportFileService reportFileService, Constants constants)
        {
            _kimbleService = kimbleService;
            _reportFileService = reportFileService;
            _constants = constants;
        }

        public async Task<List<string>> UploadData(UploadTemplateDataCommand data, CancellationToken cancellationToken)
        {
            var uploadedFiles = new List<string>();
            var schemaList = new List<string>();
            var configDataStampTemplate = new JObject();
            var completedFlag = "#Completed";
            var totalRunTime = 0.0;

            //get schema config datastamp
            if (!string.IsNullOrWhiteSpace(data.ConfigDatastamp))
            {
                configDataStampTemplate = JObject.Parse(data.ConfigDatastamp);

                //get recordTypes
                var schemas = $"{configDataStampTemplate["Schemas"]}";
                if (!string.IsNullOrWhiteSpace(schemas))
                {
                    var arrSchema = schemas.Split(",");
                    schemaList = new List<string>(arrSchema);
                }
            }

            foreach (var schema in data.SchemaList)
            {
                //check total running time; break when it's running more than the allowed threshold so as not to lose data
                if (totalRunTime >= _constants.RunTimeLimit)
                {
                    break;
                }

                //set JObject template
                JObject schemaTemplate = JObject.Parse(schema);

                //get datalake name
                var dataLakeName = $"{schemaTemplate["DataLakeName"]}";
                dataLakeName = dataLakeName.Trim().Replace(" ", "");

                //get data filename
                var dataFileName = $"{schemaTemplate["DataFileName"]}";
                if (string.IsNullOrWhiteSpace(dataFileName))
                {
                    dataFileName = dataLakeName;
                }
                dataFileName = dataFileName.Trim().Replace(" ", "");

                //set schema-name
                var schemaName = $"{dataLakeName}/{dataFileName}";

                if (schemaList.IndexOf(schemaName) >= 0)
                {
                    //get schema datastamp
                    var schemaDatastamp = $"{configDataStampTemplate[schemaName]}";

                    if (!string.IsNullOrWhiteSpace(schemaDatastamp) && schemaDatastamp != completedFlag)
                    {
                        //generate report file data stream
                        using var dataStream = new MemoryStream();
                        var requestModel = new DataRequestModel
                        {
                            SchemaName = schemaName,
                            SchemaTemplate = schemaTemplate,
                            SchemaDatastamp = schemaDatastamp,
                            TotalRunTime = totalRunTime
                        };

                        //build report data stream
                        var buildDataResponse = await BuildReportDataStream(dataStream, requestModel, cancellationToken);

                        //check run-out-time-stats, break if it's running out time
                        if (buildDataResponse.RunOutTime)
                        {
                            totalRunTime = buildDataResponse.RunTime;
                            break;
                        }

                        //start timer for the other processes as well
                        var timer = Stopwatch.StartNew();

                        //construct key name
                        var keyName = $"{dataLakeName}/kimble_{dataFileName}.csv";

                        //upload data
                        if (buildDataResponse.HasData)
                        {
                            Console.Write($"Uploading {dataLakeName} file ({keyName}) ...");

                            //upload file to S3 bucket
                            var uploaded = await _reportFileService.UploadFile(dataStream, keyName);

                            if (uploaded)
                            {
                                Console.WriteLine($" file is uploaded successfully.");

                                uploadedFiles.Add(keyName);
                            }
                        }
                        else
                        {
                            uploadedFiles.Add($"NoData_{keyName}");
                        }

                        //update config datastamp
                        var schemaStatusDatastamp = buildDataResponse.RunOutTime || buildDataResponse.LastRunHasErrors ? "#Running" : completedFlag;
                        await _reportFileService.UpdateSchemaDatastamp(schemaName, schemaStatusDatastamp).ConfigureAwait(false);

                        //stop timer
                        timer.Stop();

                        //increment total runtime
                        totalRunTime = buildDataResponse.RunTime + timer.ElapsedMilliseconds;
                    }
                }
            }

            //log total runtime
            var extractionTimeLapse = TimeSpan.FromMilliseconds(totalRunTime).Humanize(4);
            Console.WriteLine("--------------------------------------------------------------------------");
            Console.WriteLine($"Data extraction was completed in - {extractionTimeLapse}.");
            Console.WriteLine("--------------------------------------------------------------------------");

            return uploadedFiles;
        }

        #region Private Methods

        private async Task<BuildReportDataResponseModel> BuildReportDataStream(MemoryStream dataStream, DataRequestModel request, CancellationToken cancellationToken)
        {
            //int build report response
            var buildReportResponse = new BuildReportDataResponseModel
            {
                HasData = false,
                RunOutTime = false,
                RunTime = request.TotalRunTime
            };

            try
            {
                //set current schema config datastamp to #Running
                await _reportFileService.UpdateSchemaDatastamp(request.SchemaName, "#Running").ConfigureAwait(false);

                //get Kimble data
                var kimbleData = await _kimbleService.GetData(request, cancellationToken);

                //set data table
                var dataTable = kimbleData.Data;

                //set response values
                buildReportResponse.RunOutTime = kimbleData.RunOutTime;
                buildReportResponse.RunTime += kimbleData.Runtime;
                buildReportResponse.HasData = dataTable.Columns.Count > 0;
                buildReportResponse.LastRunHasErrors = kimbleData.HasErrors;

                if (dataTable?.Rows.Count > 0)
                {
                    //generate CSV content
                    var csvContent = GenerateCSVString(dataTable);

                    //convert data to memory stream
                    byte[] memstring = new UTF8Encoding(true).GetBytes(csvContent);
                    dataStream.Write(memstring, 0, memstring.Length);
                }

                return buildReportResponse;
            }
            catch (Exception ex)
            {
                throw new Exception($"Unable to build CSV report datastream. Error Message: {ex.Message}");
            }
        }

        private static string GenerateCSVString(DataTable dataTable, bool includeHeaderAsFirstRow = true, string separator = ",")
        {
            var csvRows = new StringBuilder("");
            var row = "";

            try
            {
                //set column count
                var columnCount = dataTable.Columns.Count;

                //create Header
                if (includeHeaderAsFirstRow)
                {
                    for (int index = 0; index < columnCount; index++)
                    {
                        row += dataTable.Columns[index];

                        if (index < columnCount - 1)
                        {
                            row += (separator);
                        }
                    }

                    row += (Environment.NewLine);
                }

                csvRows.Append(row);

                //loop rows
                for (var rowIndex = 0; rowIndex < dataTable.Rows.Count; rowIndex++)
                {
                    row = "";

                    for (var index = 0; index < columnCount; index++)
                    {
                        var value = dataTable.Rows[rowIndex][index].ToString();

                        //if type of field is string
                        if (dataTable.Rows[rowIndex][index] is string)
                        {
                            //if double quotes are used in value, ensure each are replaced by double quotes.
                            if (value.IndexOf("\"") >= 0)
                            {
                                value = value.Replace("\"", "\"\"");
                            }

                            //if separtor are is in value, ensure it is put in double quotes.
                            if (value.IndexOf(separator) >= 0)
                            {
                                value = "\"" + value + "\"";
                            }

                            //if string contain new line character
                            while (value.Contains("\r"))
                            {
                                value = value.Replace("\r", "");
                            }

                            while (value.Contains("\n"))
                            {
                                value = value.Replace("\n", "");
                            }
                        }

                        //use the field type of the header row as its always populated
                        var fieldType = dataTable.Rows[0][index].GetType();

                        if (fieldType == typeof(System.DateTimeOffset))
                        {
                            var thisDate = (DateTimeOffset)dataTable.Rows[rowIndex][index];
                            value = $"{thisDate.ToUnixTimeMilliseconds()}";

                            if (value == "")
                            {
                                value = "0";
                            }
                        }

                        if (fieldType == typeof(System.DateTime))
                        {
                            try
                            {
                                var thisDate = (DateTime)dataTable.Rows[rowIndex][index];
                                value = $"{((DateTimeOffset)thisDate).ToUnixTimeMilliseconds()}";
                            }
                            catch
                            {
                                value = "0";
                            }
                        }

                        row += value;

                        if (index < columnCount - 1)
                        {
                            row += separator;
                        }
                    }

                    dataTable.Rows[rowIndex][columnCount - 1].ToString().ToString().Replace(separator, " ");

                    row += Environment.NewLine;

                    csvRows.Append(row);
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"Unable to convert data into CSV-string. Error Message: {ex.Message}");
            }

            return csvRows.ToString();
        }

        #endregion Private Methods
    }
}