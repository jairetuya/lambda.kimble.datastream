﻿using Amazon.Runtime;
using KimbleDatastream.Infrastructure.Configs;
using System;

namespace KimbleDatastream.Infrastructure.Factories
{
    public interface IAwsClientFactory<out T>
    {
        T GetAwsClient();
    }

    public class AwsClientFactory<T> : IAwsClientFactory<T> where T : AmazonServiceClient, new()
    {
        private readonly IAwsConfig _awsConfig;

        public AwsClientFactory(AwsConfig awsConfig)
        {
            _awsConfig = awsConfig;
        }

        public T GetAwsClient()
        {
            return string.IsNullOrEmpty(_awsConfig.AccessKey) || string.IsNullOrEmpty(_awsConfig.SecretKey)
                ? (T)Activator.CreateInstance(typeof(T))
                : (T)Activator.CreateInstance(typeof(T), _awsConfig.GetAwsCredentials(), _awsConfig.RegionEndpoint);
        }
    }
}