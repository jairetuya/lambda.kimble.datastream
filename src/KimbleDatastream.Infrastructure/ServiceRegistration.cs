﻿using KimbleDatastream.Application.Interfaces;
using KimbleDatastream.Infrastructure.Factories;
using KimbleDatastream.Infrastructure.Services.Client;
using KimbleDatastream.Infrastructure.Services.Files;
using KimbleDatastream.Infrastructure.Storage;
using Microsoft.Extensions.DependencyInjection;
using Polly;
using Polly.Extensions.Http;
using System;
using System.Net.Http;

namespace KimbleDatastream.Infrastructure
{
    public static class ServiceRegistration
    {
        public static IServiceCollection AddInfrastructure(this IServiceCollection services)
        {
            services
                .AddTransient(typeof(IAwsClientFactory<>), typeof(AwsClientFactory<>))
                .AddTransient<IReportFileService, S3ReportFileService>()
                .AddTransient<IKimbleService, KimbleService>()
                .AddTransient<IParquetFileService, ParquetFileService>()
                .AddTransient<ICsvFileService, CsvFileService>();

            services
                .AddHttpClient<IKimbleHttpClient, KimbleHttpClient>()
                .AddPolicyHandler(GetRetryPolicy());

            return services;
        }

        private static IAsyncPolicy<HttpResponseMessage> GetRetryPolicy()
        {
            return HttpPolicyExtensions
                .HandleTransientHttpError()
                .OrResult(msg => msg.StatusCode == System.Net.HttpStatusCode.NotFound)
                .WaitAndRetryAsync(5, retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)));
        }
    }
}