﻿namespace KimbleDatastream.Infrastructure.Configs
{
    public class KimbleConfig
    {
        public string AuthenticationMode { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public string ClientId { get; set; }

        public string ClientSecret { get; set; }

        public string IssuerKey { get; set; }

        public string ServiceUri { get; set; }

        public string TokenEndpoint { get; set; }

        public string DataEndpoint { get; set; }
    }
}