﻿using System.Text.Json.Serialization;

namespace KimbleDatastream.Infrastructure.Configs
{
    public class KimbleToken
    {
        [JsonPropertyName("access_token")]
        public string AccessToken { get; set; }

        [JsonPropertyName("instance_url")]
        public string InstanceUrl { get; set; }

        [JsonPropertyName("token_type")]
        public string TokenType { get; set; }

        [JsonPropertyName("signature")]
        public string Signature { get; set; }
    }
}