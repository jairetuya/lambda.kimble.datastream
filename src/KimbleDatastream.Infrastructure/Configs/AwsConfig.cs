﻿using Amazon;
using Amazon.Runtime;
using System;
using System.Linq;
using System.Text.Json.Serialization;

namespace KimbleDatastream.Infrastructure.Configs
{
    public interface IAwsConfig
    {
        string SecretKey { get; }

        string AccessKey { get; }

        RegionEndpoint RegionEndpoint { get; }

        BasicAWSCredentials GetAwsCredentials();
    }

    public class AwsConfig : IAwsConfig
    {
        public string AccessKey { get; set; }

        public string SecretKey { get; set; }

        public string Region { get; set; }

        public BasicAWSCredentials GetAwsCredentials()
        {
            if (string.IsNullOrEmpty(AccessKey) || string.IsNullOrEmpty(SecretKey)) return null;

            return new BasicAWSCredentials(AccessKey, SecretKey);
        }

        [JsonIgnore]
        public RegionEndpoint RegionEndpoint
        {
            get
            {
                return RegionEndpoint.EnumerableAllRegions.FirstOrDefault(x =>
                    x.SystemName.Equals(Region, StringComparison.InvariantCultureIgnoreCase));
            }
        }
    }
}