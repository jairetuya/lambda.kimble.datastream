﻿using Amazon.Lambda.TestUtilities;
using KimbleDatastream.Application.Configs;
using KimbleDatastream.Application.Interfaces;
using KimbleDatastream.Application.Items.Commands.Requests;
using KimbleDatastream.Functions;
using KimbleDatastream.Infrastructure.Configs;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace KimbleDatastream.Tests.Functions
{
    [TestClass]
    public class DataExtractorFunctionTest : TestBase
    {
        #region Private Fields

        private DataExtractorFunction _sut;
        private readonly IServiceCollection _serviceCollection;
        private readonly Mock<IKimbleService> _mockKimbleService;
        private readonly Mock<IReportFileService> _mockReportFileService;
        private readonly Mock<IParquetFileService> _mockParquetFileService;
        private readonly Mock<ICsvFileService> _mockCsvFileService;
        private ReportConfigs _reportConfigs;
        private static string _defaultMessage = "The Kimble data extractor run successfully.";

        #endregion Private Fields

        #region Constructor

        public DataExtractorFunctionTest()
        {
            _serviceCollection = Startup.BuildContainer();

            _mockKimbleService = CreateMock<IKimbleService>();
            _serviceCollection.Replace(
                new ServiceDescriptor(typeof(IKimbleService),
                _ => _mockKimbleService.Object, ServiceLifetime.Transient));

            _mockReportFileService = CreateMock<IReportFileService>();
            _serviceCollection.Replace(
                new ServiceDescriptor(typeof(IReportFileService),
                _ => _mockReportFileService.Object, ServiceLifetime.Transient));

            _mockParquetFileService = CreateMock<IParquetFileService>();
            _serviceCollection.Replace(
                new ServiceDescriptor(typeof(IParquetFileService),
                _ => _mockParquetFileService.Object, ServiceLifetime.Transient));

            _mockCsvFileService = CreateMock<ICsvFileService>();
            _serviceCollection.Replace(
                new ServiceDescriptor(typeof(ICsvFileService),
                _ => _mockCsvFileService.Object, ServiceLifetime.Transient));
        }

        #endregion Constructor

        [TestInitialize]
        public void Init()
        {
            var kimbleConfig = new KimbleConfig
            {
                AuthenticationMode = "PWD",

                Username = RandomGenerator.Generate<string>(),
                Password = RandomGenerator.Generate<string>(),
                ClientId = RandomGenerator.Generate<string>(),
                ClientSecret = RandomGenerator.Generate<string>(),
                ServiceUri = RandomGenerator.Generate<string>(),
                TokenEndpoint = RandomGenerator.Generate<string>()
            };

            _serviceCollection.Replace(
                new ServiceDescriptor(typeof(KimbleConfig),
                _ => kimbleConfig, ServiceLifetime.Singleton));

            _reportConfigs = new ReportConfigs
            {
                FileType = "Parquet",
                BucketName = RandomGenerator.Generate<string>(),
                SchemaFiles = RandomGenerator.Generate<string>(),
                ParameterStoreName = RandomGenerator.Generate<string>(),
                SchemaConfigParameterStoreName = RandomGenerator.Generate<string>()
            };

            _serviceCollection.Replace(
                new ServiceDescriptor(typeof(ReportConfigs),
                _ => _reportConfigs, ServiceLifetime.Singleton));
        }

        [TestMethod]
        public async Task Run_Datastream_OneSchema_CorrectTemplate_Query_ParquetFile_Test()
        {
            // Arrange
            var templateCount = 1;
            var dataLakeName = "Resources";
            var dataFileName = "Resources_Products";
            var callType = "Query";
            var validTemplate = true;

            //setup report file service: get schema list
            var schemaDataResponse = GenerateSchemaTemplateList(templateCount, dataLakeName, dataFileName, callType, validTemplate);

            //mock report file service GetSchemaList()
            _mockReportFileService
                .Setup(_ => _.GetSchemaList())
                .ReturnsAsync((schemaDataResponse.SchemaTemplateList, schemaDataResponse.SchemaConfigDatastamp))
                .Verifiable();

            //set generated files
            var generatedFiles = new List<string>
            {
                $"{dataLakeName}/kimble_{dataFileName}.parquet"
            };

            _mockParquetFileService
                .Setup(_ => _.UploadData(
                    It.IsAny<UploadTemplateDataCommand>(),
                    It.IsAny<CancellationToken>()))
                .ReturnsAsync(generatedFiles)
                .Verifiable();

            // Act
            _sut = new DataExtractorFunction(_serviceCollection.BuildServiceProvider());
            var result = await _sut.Run(new TestLambdaContext());

            // Assert
            Assert.AreEqual(result, $"{_defaultMessage} The following ({templateCount}) files are uploaded: {string.Join(", ", generatedFiles)}");

            _mockReportFileService.Verify(_ =>
               _.GetSchemaList(),
                   Times.Once);

            _mockParquetFileService.Verify(_ =>
                _.UploadData(
                    It.IsAny<UploadTemplateDataCommand>(),
                    It.IsAny<CancellationToken>()),
                    Times.Once);
        }

        [TestMethod]
        public async Task Run_Datastream_OneSchema_InvalidTemplate_Query_ParquetFile_Test()
        {
            // Arrange
            var templateCount = 1;
            var dataLakeName = "Resources";
            var dataFileName = "Resources_Products";
            var callType = "Query";
            var validTemplate = false;

            //setup report file service: get schema list
            var schemaDataResponse = GenerateSchemaTemplateList(templateCount, dataLakeName, dataFileName, callType, validTemplate);

            //mock report file service GetSchemaList()
            _mockReportFileService
                .Setup(_ => _.GetSchemaList())
                .ReturnsAsync((schemaDataResponse.SchemaTemplateList, schemaDataResponse.SchemaConfigDatastamp))
                .Verifiable();

            //set generated files
            var generatedFiles = new List<string>();

            _mockParquetFileService
                .Setup(_ => _.UploadData(
                    It.IsAny<UploadTemplateDataCommand>(),
                    It.IsAny<CancellationToken>()))
                .ReturnsAsync(generatedFiles)
                .Verifiable();

            // Act
            _sut = new DataExtractorFunction(_serviceCollection.BuildServiceProvider());
            var result = await _sut.Run(new TestLambdaContext());

            // Assert
            Assert.AreEqual(result, $"{_defaultMessage} No files uploaded.");

            _mockReportFileService.Verify(_ =>
               _.GetSchemaList(),
                   Times.Once);
        }

        [TestMethod]
        public async Task Run_Datastream_MultipleSchema_CorrectTemplate_Query_ParquetFile_Test()
        {
            // Arrange
            var templateCount = 10;
            var dataLakeName = "Resources";
            var dataFileName = "Resources_Products";
            var callType = "Query";
            var validTemplate = true;

            //setup report file service: get schema list
            var schemaDataResponse = GenerateSchemaTemplateList(templateCount, dataLakeName, dataFileName, callType, validTemplate);

            //mock report file service GetSchemaList()
            _mockReportFileService
                .Setup(_ => _.GetSchemaList())
                .ReturnsAsync((schemaDataResponse.SchemaTemplateList, schemaDataResponse.SchemaConfigDatastamp))
                .Verifiable();

            //set generated files
            var generatedFiles = new List<string>();
            for (var t = 0; t < templateCount; t++)
            {
                generatedFiles.Add($"{dataLakeName}/{dataLakeName}_{dataFileName}.parquet");
            }

            _mockParquetFileService
                .Setup(_ => _.UploadData(
                    It.IsAny<UploadTemplateDataCommand>(),
                    It.IsAny<CancellationToken>()))
                .ReturnsAsync(generatedFiles)
                .Verifiable();

            // Act
            _sut = new DataExtractorFunction(_serviceCollection.BuildServiceProvider());
            var result = await _sut.Run(new TestLambdaContext());

            // Assert
            Assert.AreEqual(result, $"{_defaultMessage} The following ({templateCount}) files are uploaded: {string.Join(", ", generatedFiles)}");

            _mockReportFileService.Verify(_ =>
               _.GetSchemaList(),
                   Times.Once);

            _mockParquetFileService.Verify(_ =>
                _.UploadData(
                    It.IsAny<UploadTemplateDataCommand>(),
                    It.IsAny<CancellationToken>()),
                    Times.Once);
        }

        [TestMethod]
        public async Task Run_Datastream_OneSchema_CorrectTemplate_Query_CsvFile_Test()
        {
            // Arrange
            var templateCount = 1;
            var dataLakeName = "Resources";
            var dataFileName = "Resources_Products";
            var callType = "Query";
            var validTemplate = true;

            _reportConfigs = new ReportConfigs
            {
                FileType = "CSV",
                BucketName = RandomGenerator.Generate<string>(),
                SchemaFiles = RandomGenerator.Generate<string>(),
                ParameterStoreName = RandomGenerator.Generate<string>(),
                SchemaConfigParameterStoreName = RandomGenerator.Generate<string>()
            };

            _serviceCollection.Replace(
                new ServiceDescriptor(typeof(ReportConfigs),
                _ => _reportConfigs, ServiceLifetime.Singleton));

            //setup report file service: get schema list
            var schemaDataResponse = GenerateSchemaTemplateList(templateCount, dataLakeName, dataFileName, callType, validTemplate);

            //mock report file service GetSchemaList()
            _mockReportFileService
                .Setup(_ => _.GetSchemaList())
                .ReturnsAsync((schemaDataResponse.SchemaTemplateList, schemaDataResponse.SchemaConfigDatastamp))
                .Verifiable();

            //set generated files
            var generatedFiles = new List<string>
            {
                $"{dataLakeName}/kimble_{dataFileName}.csv"
            };

            _mockCsvFileService
                .Setup(_ => _.UploadData(
                    It.IsAny<UploadTemplateDataCommand>(),
                    It.IsAny<CancellationToken>()))
                .ReturnsAsync(generatedFiles)
                .Verifiable();

            // Act
            _sut = new DataExtractorFunction(_serviceCollection.BuildServiceProvider());
            var result = await _sut.Run(new TestLambdaContext());

            // Assert
            Assert.AreEqual(result, $"{_defaultMessage} The following ({templateCount}) files are uploaded: {string.Join(", ", generatedFiles)}");

            _mockReportFileService.Verify(_ =>
               _.GetSchemaList(),
                   Times.Once);

            _mockCsvFileService.Verify(_ =>
                _.UploadData(
                    It.IsAny<UploadTemplateDataCommand>(),
                    It.IsAny<CancellationToken>()),
                    Times.Once);
        }

        [TestMethod]
        public async Task Run_Datastream_MultipleSchema_CorrectTemplate_Query_CsvFile_Test()
        {
            // Arrange
            var templateCount = 10;
            var dataLakeName = "Resources";
            var dataFileName = "Resources_Products";
            var callType = "Query";
            var validTemplate = true;

            _reportConfigs = new ReportConfigs
            {
                FileType = "CSV",
                BucketName = RandomGenerator.Generate<string>(),
                SchemaFiles = RandomGenerator.Generate<string>(),
                ParameterStoreName = RandomGenerator.Generate<string>(),
                SchemaConfigParameterStoreName = RandomGenerator.Generate<string>()
            };

            _serviceCollection.Replace(
                new ServiceDescriptor(typeof(ReportConfigs),
                _ => _reportConfigs, ServiceLifetime.Singleton));

            //setup report file service: get schema list
            var schemaDataResponse = GenerateSchemaTemplateList(templateCount, dataLakeName, dataFileName, callType, validTemplate);

            //mock report file service GetSchemaList()
            _mockReportFileService
                .Setup(_ => _.GetSchemaList())
                .ReturnsAsync((schemaDataResponse.SchemaTemplateList, schemaDataResponse.SchemaConfigDatastamp))
                .Verifiable();

            //set generated files
            var generatedFiles = new List<string>();
            for (var t = 0; t < templateCount; t++)
            {
                generatedFiles.Add($"{dataLakeName}/{dataLakeName}_{dataFileName}.csv");
            }

            _mockCsvFileService
                .Setup(_ => _.UploadData(
                    It.IsAny<UploadTemplateDataCommand>(),
                    It.IsAny<CancellationToken>()))
                .ReturnsAsync(generatedFiles)
                .Verifiable();

            // Act
            _sut = new DataExtractorFunction(_serviceCollection.BuildServiceProvider());
            var result = await _sut.Run(new TestLambdaContext());

            // Assert
            Assert.AreEqual(result, $"{_defaultMessage} The following ({templateCount}) files are uploaded: {string.Join(", ", generatedFiles)}");

            _mockReportFileService.Verify(_ =>
               _.GetSchemaList(),
                   Times.Once);

            _mockCsvFileService.Verify(_ =>
                _.UploadData(
                    It.IsAny<UploadTemplateDataCommand>(),
                    It.IsAny<CancellationToken>()),
                    Times.Once);
        }

        [TestMethod]
        public async Task Run_Datastream_NoSchemaFiles_Test()
        {
            // Arrange

            _mockReportFileService
                .Setup(_ => _.GetSchemaList())
                .ReturnsAsync((new List<string>(), string.Empty))
                .Verifiable();

            // Act
            _sut = new DataExtractorFunction(_serviceCollection.BuildServiceProvider());
            var result = await _sut.Run(new TestLambdaContext());

            // Assert
            Assert.AreEqual(result, $"{_defaultMessage} No files uploaded.");
        }
    }
}