﻿using KimbleDatastream.Application.Constants;
using KimbleDatastream.Application.Interfaces;
using KimbleDatastream.Application.Models;
using KimbleDatastream.Infrastructure.Configs;
using KimbleDatastream.Infrastructure.Services.Client;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Newtonsoft.Json.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace KimbleDatastream.Tests.Infrastructure
{
    [TestClass]
    public class KimbleServiceTest : TestBase
    {
        private KimbleService _sut;
        private readonly Mock<IKimbleHttpClient> _mockKimbleHttpClient;
        private KimbleConfig _kimbleConfig;
        private Constants _constants;

        public KimbleServiceTest()
        {
            _mockKimbleHttpClient = CreateMock<IKimbleHttpClient>();
        }

        [TestInitialize]
        public void Init()
        {
            _kimbleConfig = new KimbleConfig
            {
                AuthenticationMode = "JWT",
                Username = RandomGenerator.Generate<string>(),
                Password = RandomGenerator.Generate<string>(),
                ClientId = RandomGenerator.Generate<string>(),
                ClientSecret = RandomGenerator.Generate<string>(),
                ServiceUri = RandomGenerator.Generate<string>(),
                TokenEndpoint = RandomGenerator.Generate<string>()
            };

            _constants = new Constants
            {
                RunTimeLimit = 14.0 * 60 * 1000,
            };
        }

        [TestMethod]
        public async Task GetKimbleData_HappyPass_Test()
        {
            // Arrange
            var templateCount = 1;
            var dataLakeName = "Resources";
            var dataFileName = "Resources_Products";
            var callType = "Query";
            var validTemplate = true;

            //setup report file service: get schema list
            var schemaData = GenerateSchemaTemplateList(templateCount, dataLakeName, dataFileName, callType, validTemplate, "#NotStarted");

            //set JObject template
            JObject schemaTemplate = JObject.Parse(schemaData.SchemaTemplateList[0]);

            //set upload data command
            var dataRequestModel = new DataRequestModel
            {
                SchemaName = $"{dataLakeName}/{dataFileName}",
                SchemaTemplate = schemaTemplate,
                SchemaDatastamp = "#NotStarted",
                TotalRunTime = 0.0
            };

            var responseString = "{\"totalSize\":2,\"done\":true,\"records\":[{\"attributes\":{\"type\":\"KimbleOne__ExpenseClaim__c\",\"url\":\" / services / data / v52.0 / sobjects / KimbleOne__ExpenseClaim__c / a0k4G00000q3iNeQAI\"},\"Id\":\"a0k4G00000q3iNeQAI\",\"CreatedById\":\"0054G00000B7a9mQAB\",\"CreatedDate\":\"2021 - 03 - 08T05: 19:23.000 + 0000\",\"CurrencyIsoCode\":\"GBP\",\"KimbleOne__ApprovalStatus__c\":\"Approved\",\"KimbleOne__ApprovedDate__c\":\"2021 - 03 - 08\",\"KimbleOne__Approver1User__c\":\"005D00000027CFMIA2\",\"KimbleOne__ForecastingTimePeriod__c\":\"a1rD0000004pAh9IAE\",\"KimbleOne__Reference__c\":\"EXC021861\",\"KimbleOne__ReimbursementCcyTotalReimbursementAmount__c\":10961.0,\"KimbleOne__ReimbursementCurrencyIsoCode__c\":\"PHP\",\"KimbleOne__ResourcedActivity__c\":\"a1a4G00000A5YxLQAV\",\"KimbleOne__Resource__c\":\"a1X4G000003ESgAUAW\",\"KimbleOne__Source__c\":\"a1RD00000049pBHMAY\",\"KimbleOne__Status__c\":\"a1RD0000002CdG6MAK\",\"KimbleOne__TotalExpenseCost__c\":166.28,\"KimbleOne__TotalReimbursementAmount__c\":166.28,\"LastModifiedById\":\"005D0000001gBuzIAE\",\"LastModifiedDate\":\"2021 - 04 - 07T15: 27:33.000 + 0000\",\"OwnerId\":\"0054G00000B7a9mQAB\"},{\"attributes\":{\"type\":\"KimbleOne__ExpenseClaim__c\",\"url\":\" / services / data / v52.0 / sobjects / KimbleOne__ExpenseClaim__c / a0k4G00000bfrboQAA\"},\"Id\":\"a0k4G00000bfrboQAA\",\"CreatedById\":\"0054G00000B7a9mQAB\",\"CreatedDate\":\"2019 - 07 - 12T13: 31:51.000 + 0000\",\"CurrencyIsoCode\":\"GBP\",\"KimbleOne__ApprovalStatus__c\":\"Approved\",\"KimbleOne__ApprovedDate__c\":\"2019 - 07 - 16\",\"KimbleOne__Approver1User__c\":\"005D0000002IQjUIAW\",\"KimbleOne__ForecastingTimePeriod__c\":\"a1rD0000004pAgpIAE\",\"KimbleOne__Reference__c\":\"EXC017190\",\"KimbleOne__ReimbursementCcyTotalReimbursementAmount__c\":97900.0,\"KimbleOne__ReimbursementCurrencyIsoCode__c\":\"PHP\",\"KimbleOne__ResourcedActivity__c\":\"a1aD00000001jH9IAI\",\"KimbleOne__Resource__c\":\"a1X4G000003ESgAUAW\",\"KimbleOne__Source__c\":\"a1RD00000049pBHMAY\",\"KimbleOne__Status__c\":\"a1RD0000002CdG6MAK\",\"KimbleOne__TotalExpenseCost__c\":1490.82,\"KimbleOne__TotalReimbursementAmount__c\":1490.82,\"LastModifiedById\":\"005D0000001gBuzIAE\",\"LastModifiedDate\":\"2019 - 08 - 08T14: 47:41.000 + 0000\",\"OwnerId\":\"0054G00000B7a9mQAB\"}]}";
            _mockKimbleHttpClient
                .Setup(_ => _.GetResponseString(
                    It.IsAny<string>(),
                    It.IsAny<CancellationToken>()))
                .ReturnsAsync(responseString)
                .Verifiable();

            // Act
            _sut = new KimbleService(_mockKimbleHttpClient.Object, _kimbleConfig, _constants);

            var result = await _sut.GetData(dataRequestModel, default);

            // Assert
            Assert.IsInstanceOfType(result, typeof(DataResponseModel));
            Assert.IsTrue(result.Data.Rows.Count > 0);
            Assert.IsFalse(result.RunOutTime);
            Assert.IsTrue(result.Runtime > 0);

            _mockKimbleHttpClient.Verify(_ =>
                _.GetResponseString(
                    It.IsAny<string>(),
                    It.IsAny<CancellationToken>()),
                    Times.Once);
        }
    }
}