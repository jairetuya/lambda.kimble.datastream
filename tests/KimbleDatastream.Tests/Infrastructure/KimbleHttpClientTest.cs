﻿using KimbleDatastream.Application.Interfaces;
using KimbleDatastream.Infrastructure.Configs;
using KimbleDatastream.Infrastructure.Services.Client;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Moq.Protected;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace KimbleDatastream.Tests.Infrastructure
{
    [TestClass]
    public class KimbleHttpClientTest : TestBase
    {
        private KimbleHttpClient _sut;
        private HttpClient _httpClient;
        private readonly Mock<IReportFileService> _mockReportFileService;
        private KimbleConfig _kimbleConfig;

        public KimbleHttpClientTest()
        {
            _mockReportFileService = CreateMock<IReportFileService>();
        }

        [TestInitialize]
        public void Init()
        {
            var responseContent = "{\"access_token\":\"00D2z0000008aZ3!AQwAQHufe2J1gWD87LzD3Q8fZy9LQf.Q0TDeQsihfPLJ7BaXduLyidv6iTvVwfaO7cTCtdKXIZSKL5eSZo0OsoVqydV96ajf\",\"instance_url\":\"https://lemongrasscloud--sandbox1.my.salesforce.com\",\"id\":\"https://test.salesforce.com/id/00D2z0000008aZ3EAI/0054G00000B7a9mQAB\",\"token_type\":\"Bearer\",\"issued_at\":\"1628162642248\",\"signature\":\"AlCU5sqy2jLaQ8Wsp1+Q13MTgo3L0qNARbxF/0RNSKY=\"}";
            var handlerMock = new Mock<HttpMessageHandler>(MockBehavior.Strict);
            handlerMock
               .Protected()
               .Setup<Task<HttpResponseMessage>>(
                  "SendAsync",
                  ItExpr.IsAny<HttpRequestMessage>(),
                  ItExpr.IsAny<CancellationToken>()
               )
               .ReturnsAsync(new HttpResponseMessage()
               {
                   StatusCode = HttpStatusCode.OK,
                   Content = new StringContent(responseContent),
               })
               .Verifiable();

            _httpClient = new HttpClient(handlerMock.Object);
        }

        [TestMethod]
        public async Task GetKimbleData_PWD_HappyPass_Test()
        {
            // Arrange
            var uri = RandomGenerator.Generate<string>();

            _kimbleConfig = new KimbleConfig
            {
                AuthenticationMode = "PWD",
                Username = RandomGenerator.Generate<string>(),
                Password = RandomGenerator.Generate<string>(),
                ClientId = RandomGenerator.Generate<string>(),
                ClientSecret = RandomGenerator.Generate<string>(),
                ServiceUri = "https://test.salesforce.com",
                TokenEndpoint = "services/oauth2/token"
            };

            // Act
            _sut = new KimbleHttpClient(_httpClient, _kimbleConfig, _mockReportFileService.Object);

            var result = await _sut.GetResponseString(uri, default);

            // Assert
            Assert.IsTrue(!string.IsNullOrEmpty(result));
        }

        [TestMethod]
        public async Task GetKimbleData_JWT_HappyPass_Test()
        {
            // Arrange
            var uri = RandomGenerator.Generate<string>();

            _kimbleConfig = new KimbleConfig
            {
                AuthenticationMode = "JWT",
                Username = RandomGenerator.Generate<string>(),
                Password = RandomGenerator.Generate<string>(),
                ClientId = RandomGenerator.Generate<string>(),
                ClientSecret = RandomGenerator.Generate<string>(),
                IssuerKey = RandomGenerator.Generate<string>(),
                ServiceUri = "https://test.salesforce.com",
                TokenEndpoint = "services/oauth2/token"
            };

            //var accessKey = File.ReadAllText(@"c:\cert\server_public.pem");
            var accessKey = "-----BEGIN RSA PRIVATE KEY-----\r\nMIIEoAIBAAKCAQEAokMur8RCLas8hKVKB61nF9vKyxRCLkWiW4PNFC/0AaXLu6Ql\r\nwQVvFIROEe7gzHhErWhVdm5Pj8Z1vIWtkFdRsf2QwJmsOpyd2QY+LZsrGzuXE5e7\r\n+LdjNy+gc4QAynwfBGZNTk/eK2dA9JZuWbmqI7j8dokUbGJGBJZZ8CBaBzYZIBKw\r\nDU1oTOxpD59/q1Ph5ecPXrPkM7xXEerR1uZZVX/dYP0iLCWoCaqr8Ci/FJa/xEav\r\nuBOZ0LSXyUOfaDdgc+8do5pss9NmPkYoeZe+nXnisBzW0afsVsYVa2T59B9c5qXR\r\nU/B4KB7kJZki2gwPtq4tpXJadHDDRcrG2sjbfwIDAQABAoIBAGAiRYrFUCAWj6hW\r\n9WFPDwFHqwJeYkdf4i0uDVakgoBnZikpu2R8f688I7TkPjNmHepPIjMkb+bSwfaL\r\njKUz1qcJf7VcVnnzSGHIHRaQFB3yVDO7dhaJMDMF+J332zAcEZRdt/dpwkscxZMj\r\nfQWTxoWbl5fy5zK/lpQpL+fggvY52QllTgOXJ7uEdL+XNm/p2nXl5ZYTl08csqT/\r\ndwrHmnGn32HN/Xg5PeM8P1KePeqDO75HQun+iUeVPMzRbGIhbhqFGk4Ay9h9IPTL\r\nEVB5+BbDCSVeEKKMizDGPw//RlF16/D3PrQkdvW5Ly51/ipQb7LYB8tg8kVUPebu\r\n45hVzYECgYEA1+UOMkOM3rDlxWlFqi0kdVX8o3ynPH3Y+R0Jbsz4uC8/8RgVu9EC\r\nUr6ZcgrF0rgGkHOe4A5ADezeI2xiaPKjJMWan+r5O1pdgWysOQPT8a55GAeoByXn\r\nCBR1pSaK7YCeK4jZVNCbgWsTA4yedBXuuEaiEvSwn5zrgX1PiFT/A3ECgYEAwGef\r\n76IbL17rSft0uIPafP53wZgys6p3/fmdFM1wKpiB5pPEWvLKJzDJNjYaXlApFDhm\r\nMCBjd5hr5/5fInIBuyrDA4QTIxmG0MoIPRP0AZGYwHm6ogrEQYEZItLn/Arh6zIo\r\nRAAUkUtnx8G+Y/iURO3EkGVTbaJIqFNhnqFCde8CfzbQ8cnRXPMt9ykokYej7p3c\r\nN0lLXSr+yRh3Ru3bHUums5WDiAEoEG7x9gP+uDvIfeIveSdqdtkSS1SzMEl1UfcE\r\nNT5lauKnDxbNgOz5C9dRdW2khAVMmlxy1WwZXSzweziShpwCW3mAtlMwYQsYhoRD\r\nauNlPXNJZIJ8TrVO33ECgYAY4w5385LqnK94gVMGrt9q/YtF+kbMoIcmzcu4rh4N\r\nFG7xAToLvY3SZegQ4AynGF1H5Ueqf/X9uTRz7J4nmXGJ4S0aXlIPXzTl+sv4AoW0\r\nUj3c36H5ZcUe5/D1ZSxmmI3CW1mD+jKu3H8PAot66ONcyxh6yPBypOQbCbX9y8QJ\r\n9wKBgCXFJNIWYCbpoztGWhibyIF1WrcsDC5L2Fqc87JNXDGX0JNIUDl0qPr12Fxh\r\n6o6mkuVt563ibaMfM6k970AyYpIoHIKrMmz1rirF1uT60iR+fMNivhUr8iJ5Gn6g\r\ncOQyrfsJpDia5f6AUNQ2B7cL82a5DfSYMFoMufHSDrGP+Plt\r\n-----END RSA PRIVATE KEY-----";

            _mockReportFileService
                .Setup(_ => _.GetSecretAccessKey())
                .ReturnsAsync(accessKey)
                .Verifiable();

            // Act
            _sut = new KimbleHttpClient(_httpClient, _kimbleConfig, _mockReportFileService.Object);

            var result = await _sut.GetResponseString(uri, default);

            // Assert
            Assert.IsTrue(!string.IsNullOrEmpty(result));
        }
    }
}