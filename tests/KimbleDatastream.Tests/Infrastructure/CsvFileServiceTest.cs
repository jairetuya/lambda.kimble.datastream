﻿using KimbleDatastream.Application.Constants;
using KimbleDatastream.Application.Interfaces;
using KimbleDatastream.Application.Items.Commands.Requests;
using KimbleDatastream.Application.Models;
using KimbleDatastream.Infrastructure.Services.Files;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Data;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace KimbleDatastream.Tests.Infrastructure
{
    [TestClass]
    public class CsvFileServiceTest : TestBase
    {
        private CsvFileService _sut;
        private readonly Mock<IKimbleService> _mockKimbleService;
        private readonly Mock<IReportFileService> _mockReportFileService;
        private Constants _constants;

        public CsvFileServiceTest()
        {
            _mockKimbleService = CreateMock<IKimbleService>();
            _mockReportFileService = CreateMock<IReportFileService>();
        }

        [TestInitialize]
        public void Init()
        {
            _constants = new Constants
            {
                RunTimeLimit = 14.0 * 60 * 1000,
            };
        }

        [TestMethod]
        public async Task UploadData_OneSchema_NotStarted_Test()
        {
            // Arrange
            var templateCount = 1;
            var dataLakeName = "Resources";
            var dataFileName = "Resources_Products";
            var callType = "Query";
            var validTemplate = true;

            //setup report file service: get schema list
            var schemaData = GenerateSchemaTemplateList(templateCount, dataLakeName, dataFileName, callType, validTemplate, "#NotStarted");

            //set upload data command
            var uploadData = new UploadTemplateDataCommand
            {
                SchemaList = schemaData.SchemaTemplateList,
                ConfigDatastamp = schemaData.SchemaConfigDatastamp
            };

            //mock report file service: UpdateDatastamp
            _mockReportFileService
                .Setup(_ => _.UpdateSchemaDatastamp(
                    It.IsAny<string>(),
                    It.IsAny<string>()))
                .ReturnsAsync("OK")
                .Verifiable();

            //mock SysAid service: GetData
            var dataResponse = new DataResponseModel
            {
                Data = GenerateDataTable(),
                RunOutTime = false,
                Runtime = RandomGenerator.Generate<double>()
            };

            _mockKimbleService
                .Setup(_ => _.GetData(
                    It.IsAny<DataRequestModel>(),
                    It.IsAny<CancellationToken>()))
                .ReturnsAsync(dataResponse)
                .Verifiable();

            _mockReportFileService
                .Setup(_ => _.UploadFile(
                        It.IsAny<MemoryStream>(),
                        It.IsAny<string>()
                    ))
                .ReturnsAsync(true)
                .Verifiable();

            // Act
            _sut = new CsvFileService(_mockKimbleService.Object, _mockReportFileService.Object, _constants);

            var result = await _sut.UploadData(uploadData, default);

            // Assert
            Assert.AreEqual(result.Count, templateCount);
            Assert.AreEqual(result[0], $"{dataLakeName}/kimble_{dataFileName}.csv");

            _mockKimbleService.Verify(_ =>
                _.GetData(
                    It.IsAny<DataRequestModel>(),
                    It.IsAny<CancellationToken>()),
                    Times.Exactly(templateCount));

            _mockReportFileService.Verify(_ =>
                _.UploadFile(
                    It.IsAny<MemoryStream>(),
                    It.IsAny<string>()),
                    Times.Exactly(templateCount));
        }

        [TestMethod]
        public async Task UploadData_OneSchema_Running_Test()
        {
            // Arrange
            var templateCount = 1;
            var dataLakeName = "Resources";
            var dataFileName = "Resources_Products";
            var callType = "Query";
            var validTemplate = true;

            //setup report file service: get schema list
            var schemaData = GenerateSchemaTemplateList(templateCount, dataLakeName, dataFileName, callType, validTemplate, "#Running");

            //set upload data command
            var uploadData = new UploadTemplateDataCommand
            {
                SchemaList = schemaData.SchemaTemplateList,
                ConfigDatastamp = schemaData.SchemaConfigDatastamp
            };

            //mock report file service: UpdateDatastamp
            _mockReportFileService
                .Setup(_ => _.UpdateSchemaDatastamp(
                    It.IsAny<string>(),
                    It.IsAny<string>()))
                .ReturnsAsync("OK")
                .Verifiable();

            //mock SysAid service: GetData
            var dataResponse = new DataResponseModel
            {
                Data = GenerateDataTable(),
                RunOutTime = false,
                Runtime = RandomGenerator.Generate<double>()
            };

            _mockKimbleService
                .Setup(_ => _.GetData(
                    It.IsAny<DataRequestModel>(),
                    It.IsAny<CancellationToken>()))
                .ReturnsAsync(dataResponse)
                .Verifiable();

            _mockReportFileService
                .Setup(_ => _.UploadFile(
                        It.IsAny<MemoryStream>(),
                        It.IsAny<string>()
                    ))
                .ReturnsAsync(true)
                .Verifiable();

            // Act
            _sut = new CsvFileService(_mockKimbleService.Object, _mockReportFileService.Object, _constants);

            var result = await _sut.UploadData(uploadData, default);

            // Assert
            Assert.AreEqual(result.Count, templateCount);
            Assert.AreEqual(result[0], $"{dataLakeName}/kimble_{dataFileName}.csv");

            _mockKimbleService.Verify(_ =>
                _.GetData(
                    It.IsAny<DataRequestModel>(),
                    It.IsAny<CancellationToken>()),
                    Times.Exactly(templateCount));

            _mockReportFileService.Verify(_ =>
                _.UploadFile(
                    It.IsAny<MemoryStream>(),
                    It.IsAny<string>()),
                    Times.Exactly(templateCount));
        }

        [TestMethod]
        public async Task UploadData_OneSchema_Completed_Test()
        {
            // Arrange
            var templateCount = 1;
            var dataLakeName = "Resources";
            var dataFileName = "Resources_Products";
            var callType = "Query";
            var validTemplate = true;

            //setup report file service: get schema list
            var schemaData = GenerateSchemaTemplateList(templateCount, dataLakeName, dataFileName, callType, validTemplate, "#Completed");

            //set upload data command
            var uploadData = new UploadTemplateDataCommand
            {
                SchemaList = schemaData.SchemaTemplateList,
                ConfigDatastamp = schemaData.SchemaConfigDatastamp
            };

            // Act
            _sut = new CsvFileService(_mockKimbleService.Object, _mockReportFileService.Object, _constants);

            var result = await _sut.UploadData(uploadData, default);

            // Assert
            Assert.AreEqual(result.Count, 0);
        }

        [TestMethod]
        public async Task UploadData_MultipleSchema_NotStarted_Test()
        {
            // Arrange
            var templateCount = 10;
            var dataLakeName = "Resources";
            var dataFileName = "Resources_Products";
            var callType = "Query";
            var validTemplate = true;

            //setup report file service: get schema list
            var schemaData = GenerateSchemaTemplateList(templateCount, dataLakeName, dataFileName, callType, validTemplate, "#NotStarted");

            //set upload data command
            var uploadData = new UploadTemplateDataCommand
            {
                SchemaList = schemaData.SchemaTemplateList,
                ConfigDatastamp = schemaData.SchemaConfigDatastamp
            };

            //mock report file service: UpdateDatastamp
            _mockReportFileService
                .Setup(_ => _.UpdateSchemaDatastamp(
                    It.IsAny<string>(),
                    It.IsAny<string>()))
                .ReturnsAsync("OK")
                .Verifiable();

            //mock SysAid service: GetData
            var dataResponse = new DataResponseModel
            {
                Data = GenerateDataTable(),
                RunOutTime = false,
                Runtime = RandomGenerator.Generate<double>()
            };

            _mockKimbleService
                .Setup(_ => _.GetData(
                    It.IsAny<DataRequestModel>(),
                    It.IsAny<CancellationToken>()))
                .ReturnsAsync(dataResponse)
                .Verifiable();

            _mockReportFileService
                .Setup(_ => _.UploadFile(
                        It.IsAny<MemoryStream>(),
                        It.IsAny<string>()
                    ))
                .ReturnsAsync(true)
                .Verifiable();

            // Act
            _sut = new CsvFileService(_mockKimbleService.Object, _mockReportFileService.Object, _constants);

            var result = await _sut.UploadData(uploadData, default);

            // Assert
            Assert.AreEqual(result.Count, templateCount);

            for (var x = 0; x <= templateCount - 1; x++)
            {
                var generatedFile = $"{dataLakeName}/kimble_{dataFileName}.csv";

                //should exist in the return result
                Assert.IsTrue(result.IndexOf(generatedFile) >= 0);
            }

            _mockKimbleService.Verify(_ =>
                _.GetData(
                    It.IsAny<DataRequestModel>(),
                    It.IsAny<CancellationToken>()),
                    Times.Exactly(templateCount));

            _mockReportFileService.Verify(_ =>
                _.UploadFile(
                    It.IsAny<MemoryStream>(),
                    It.IsAny<string>()),
                    Times.Exactly(templateCount));
        }

        [TestMethod]
        public async Task UploadData_MultipleSchema_Completed_Test()
        {
            // Arrange
            var templateCount = 10;
            var dataLakeName = "Resources";
            var dataFileName = "Resources_Products";
            var callType = "Query";
            var validTemplate = true;

            //setup report file service: get schema list
            var schemaData = GenerateSchemaTemplateList(templateCount, dataLakeName, dataFileName, callType, validTemplate, "#Completed");

            //set upload data command
            var uploadData = new UploadTemplateDataCommand
            {
                SchemaList = schemaData.SchemaTemplateList,
                ConfigDatastamp = schemaData.SchemaConfigDatastamp
            };

            // Act
            _sut = new CsvFileService(_mockKimbleService.Object, _mockReportFileService.Object, _constants);

            var result = await _sut.UploadData(uploadData, default);

            // Assert
            Assert.AreEqual(result.Count, 0);
        }

        #region Helpers

        private DataTable GenerateDataTable()
        {
            var dataTable = new DataTable();
            dataTable.Columns.Add("Id", typeof(int));
            dataTable.Columns.Add("Name", typeof(string));

            dataTable.Rows.Add($"{RandomGenerator.Generate<int>()}", RandomGenerator.Generate<string>());

            return dataTable;
        }

        #endregion Helpers
    }
}