using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Spackle;
using System.Collections.Generic;
using System.Text;

namespace KimbleDatastream.Tests
{
    public abstract class TestBase
    {
        protected RandomObjectGenerator RandomGenerator { get; }
        private readonly List<Mock> _mocks;

        protected TestBase()
        {
            RandomGenerator = new RandomObjectGenerator();
            _mocks = new List<Mock>();
        }

        protected Mock<T> CreateMock<T>()
            where T : class
        {
            var ret = new Mock<T>(MockBehavior.Strict);

            _mocks.Add(ret);

            return ret;
        }

        [TestCleanup]
        public void BaseCleanup()
        {
            foreach (var mock in _mocks)
            {
                mock.VerifyAll();
            }
        }

        protected SchemaResponse GenerateSchemaTemplateList(int templateCount, string dateLakeName = "", string dataFileName = "", string callType = "", bool validTemplate = false, string status = "#NotStarted")
        {
            var schemaTemplateList = new List<string>();
            var schemaList = new List<string>();

            for (var c = 0; c < templateCount; c++)
            {
                var rndDataLakeName = validTemplate ? $"{dateLakeName}/{dataFileName}" : RandomGenerator.Generate<string>();
                var schemaTemplate = "{" + $"\"DataLakeName\": \"{dateLakeName}\",\"DataFileName\": \"{dataFileName}\",\"Version\": \"v52.0\",  \"CallType\": \"{callType}\", \"Endpoint\" : \"\",  \"Object\": \"KimbleOne__Resource__c\",  \"Fields\": [\"Id\",\"KimbleOne__BusinessUnit__c\",\"KimbleOne__LastName__c\",\"KimbleOne__FirstName__c\",\"LG_JobTitle__c\",\"LG_ReportsTo__c\",\"Capability__c\",\"Capability_Group__c\"], \"Where\" : [[\"Capability_Group__c\", \"=\", \"Products\", \"string\"]]" + "}";

                if (!validTemplate)
                {
                    schemaTemplate = RandomGenerator.Generate<string>();
                }

                //add to schemas
                schemaList.Add(rndDataLakeName);

                //add to schema templates
                schemaTemplateList.Add(schemaTemplate);
            }

            return new SchemaResponse
            {
                SchemaList = schemaList,
                SchemaTemplateList = schemaTemplateList,
                SchemaConfigDatastamp = GenerateSchemaConfigDatastamps(schemaList, status)
            };
        }

        protected string GenerateSchemaConfigDatastamps(List<string> schemaList, string status)
        {
            //compose schema config value
            var schemaConfigs = new StringBuilder();
            schemaConfigs.Append("{");
            schemaConfigs.Append($"\"Schemas\":\"{string.Join(",", schemaList)}\",");

            foreach (var s in schemaList)
            {
                schemaConfigs.Append($"\"{s}\":\"{status}\",");
            }

            schemaConfigs.Append("}");

            //init config datastamp value
            return schemaConfigs.ToString();
        }

        protected class SchemaResponse
        {
            public List<string> SchemaTemplateList { get; set; }

            public string SchemaConfigDatastamp { get; set; }

            public List<string> SchemaList { get; set; }
        }
    }
}