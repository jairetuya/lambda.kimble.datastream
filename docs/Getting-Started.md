# Getting Started
## Software requirements
* .NET **Core 3.1 SDK** from: [https://dotnet.microsoft.com/download](https://dotnet.microsoft.com/download)
* Visual Studio 2019 
* [AWS CLI v2](https://aws.amazon.com/cli/)
* Deployment Package
  * [AWS Extensions for .NET CLI](https://github.com/aws/aws-extensions-for-dotnet-cli)
  * [Serverless Framework](https://www.serverless.com/framework/docs/providers/aws/guide/installation/) - Deploying from local environment
* Local Testing (optional - only if you want to run the application locally)
  * [AWS SAM CLI](https://aws.amazon.com/serverless/sam/)
  * [Docker Engine](https://docs.docker.com/engine/install/)

## Solution structure
The serverless AWS Lambda function was developed using .NET Core 3.1 framework. Though it's a small application, it closely follows [onion or clean architecture](https://medium.com/@shivendraodean/software-architecture-the-onion-architecture-1b235bec1dec) and [CQRS](https://martinfowler.com/bliki/CQRS.html), by using [MediatR](https://github.com/jbogard/MediatR), a simple in-process implementation the Mediator pattern.

### AWS Lambda Function `(src/KimbleDatastream)`
Contains the AWS Lambda Funtion project that acts as the main project.

### Application Layer `(src/KimbleDatastream.Application)`
Acts as the core of the application and does not have any dependencies to other projects in the solution. Contains domain models, enums, interfaces, commands & queries, and helper classes.

### Infrastructure Layer `(src/KimbleDatastream.Infrastructure)`
Contains all external implementations and has dependency to Application layer.

### Unit Tests `(tests/KimbleDatastream.Tests)`
Contains unit tests for lambda function. Uses [MSTest](https://github.com/Microsoft/testfx-docs) Framework.