# Development Setup
## Local environment
* Create your own branch based on the **"main"** branch of this repo.
* Clone your newly created branch into your local machine.
* Make sure that the solution builds successfully.
* Run all the unit tests in `tests/KimbleDatastream.Tests` and ensure all unit tests passed.

## Configuration files
The application has one configuration for each environment, dev & test. `config-dev.json` & `config-dev.json`. 

#### Report configuration
Settings for report file generation.

|Name|Description|
|:---|:---|
|`REPORT_CONFIGS_FILE_TYPE`|Specifies the report file format of the generated file. The default is [Parquet](https://github.com/elastacloud/parquet-dotnet)|
|`REPORT_CONFIGS_BUCKET_NAME`|Specifies the AWS S3 bucket where the report files will be uploaded.|
|`REPORT_CONFIGS_SCHEMA_FILES`|Specifies the special folder name with which the Kimble schema files are stored in the AWS S3 bucket.|
|`REPORT_CONFIGS_PARAMETER_STORE_NAME`|Specifies the SSM Parameter Store Name with which the secret RSA key is stored.|

#### AWS credentials
Contains your own AWS security credentials that will be used when deploying this Lambda function.

|Name|Description|
|:---|:---|
|`AWS_ACCESS_KEY`|Your AWS access key ID|
|`AWS_SECRET_KEY`|Your AWS secret key|
|`AWS_REGION`|The AWS region where this lambda function will be deployed.|

#### Kimble configuration
Settings for extracting Kimble data.

|Name|Description|
|:---|:---|
|`KIMBLE_CONFIG_AUTHENTICATIONMODE`|The authentication mode with which the system uses to contact Kimble when getting access token. It can either be - PWD or JWT|
|`KIMBLE_CONFIG_USERNAME`|The Kimble Suite Login ID that will represent as the user - to be used in PWD authentication mode.|
|`KIMBLE_CONFIG_PASSWORD`|The Kimble Suite Password of the user - to be used in PWD authentication mode.|
|`KIMBLE_CONFIG_CLIENT_ID`|The Client ID issued to you through the Kimble Suite - to be used in PWD authentication mode.|
|`KIMBLE_CONFIG_CLIENT_SECRET`|The Client Secret issued to you through the Kimble Suite- to be used in PWD authentication mode.|
|`KIMBLE_CONFIG_ISSUER_KEY`|The key provided by Kimble to Lemongrass as consumer identifier - to be used in JWT authentication mode.|
|`KIMBLE_CONFIG_SERVICE_URL`|The Kimble Account URL used by Lemongrass.|
|`KIMBLE_CONFIG_TOKEN_ENDPOINT`|The endpoint provided by Kimble with which the system uses to get access token|
|`KIMBLE_CONFIG_DATA_ENDPOINT`|The endpoint provided by Kimble with which the system uses, together with the composed endpoint, to get data|

## Deployment
The application will be deployed to AWS Lambda function as a package ZIP file and uses [Serverless Framework](https://www.serverless.com/framework/docs/providers/aws/guide/installation/) to deploy, which also creates AWS infrastructure resources it requires. The serverless configurations that is used in deploying this application to AWS is in the file called **`serverless.yml`**.

#### Deployment package
Defined in the package artifact inside the **`serverless.yml`** configuration is the path to our AWS Lambda Function package in ZIP format.

We can pack the application using Amazon.Lambda.Tools from [.NET Core Global Tool](https://aws.amazon.com/blogs/developer/net-core-global-tools-for-aws/). Follow the steps below to pack the application:

Once Amazon.Lambda.Tools was installed and while on the root directory of the solution:

```
C:\LCP.OCTODashboard.KimbleDatastream\src\KimbleDatastream>
```

Assuming that the solution is in the directory above, type the command below:

```
dotnet lambda package --configuration release --framework netcoreapp3.1 --output-package bin/release/netcoreapp3.1/KimbleDatastream-package.zip

--- You should see the output below:
Lambda project successfully packaged: C:\LCP.OCTODashboard.KimbleDatastream\src\KimbleDatastream\bin\release\netcoreapp3.1\KimbleDatastream-package.zip
```
--- To deploy the application from your local envrironment to AWS, you current path should be where 'serverless_local.yml', and then type the code:
sls deploy --config serverless_local.yml

--- To remove the locally deployed AWS Lambda, type the code:
sls remove --config serverless_local.yml

--- To deploy the AWS Lamda to an <Environment>, you need to check and complete the <environment> value as indicated in the 'serverless.yml' file, then type the code:
sls deploy

If you want to deploy the application from your local envrironment to AWS, you type `serverless deploy` or `sls deploy`

## Local testing
To test the Lambda function locally, you need to do the following:
1. [Install AWS Serverless Application Model CLI](https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/serverless-sam-cli-install.html)
   + AWS SAM is an extension of CloudFormation that further simplifies the process of building serverless application resources.
2. [Install Docker Engine](https://docs.docker.com/engine/install/)
   + AWS SAM CLI requires [Docker](https://www.docker.com/) containers to simulate the AWS Lambda runtime environment on your local development environment.

The solution includes a SAM configuration template for the lambda function. Using the template requires an existing S3 bucket defined in `serverless.yml`, a valid AWS credentials and a valid Kimble settings. Each function can be invoked using the code below:

```
--- To extract and generate the report file, you need to be in the path where the 'sam-local-template.yml', and then type the code:
sam local invoke -t sam-local-template.yml DataExtractor
