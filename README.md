# Kimble Datastream for OCTO Dashboard

A serverless [AWS Lambda](https://aws.amazon.com/lambda/) function that connects to Kimble and extracts the specified data defined in Kimble schema files. Generates a report file and uploads the reports into an AWS S3 bucket. These functions are triggered to run on schedule by AWS Eventbridge.

## Background
The Kimble schema files contains the relevant information to which the extracted data will be patterned into. These extracted will then be uploaded into the target AWS S3 buckets.

![Diagram alt](docs/images/kimble-datastream-diagram.jpg "Kimble Datastream Diagram")

## Application Overview
The diagram, in the image above, shows how this application fits in into OCTO Dashboard. Components that are not part of this application in the diagram is grayed out. This AWS Lambda is scheduled to extract data from Kimble on a specific schedule:

| Function Name | Runs |Description |
|:--- |---|:---|
|every 12:30AM daily|Extracts all data (governed by the individual schema files) and generates a report file using the current date.|

As shown in the diagram above, the process starts when a specific scheduled time occurs:
1. A specific function will be triggered based on schedule.
2. Connects to an AWS S3 bucket (SchemaFiles) to retrieve the schema files.
2. Connects to Kimble and queries specific data defined in each schema files.
3. Receives the data.
4. Process, generates a report file, and uploads the file to AWS S3 bucket.

## Getting Started
1. [Software requirements](docs/getting-started.md?anchor=software-requirements)
2. [Solution structure](docs/getting-started.md?anchor=solution-structure)

## Development Setup
1. [Local environment](docs/dev-setup.md?anchor=local-environment)
2. [Configuration files](docs/dev-setup.md?anchor=configuration-files)
3. [Deployment](docs/dev-setup.md?anchor=deployment)
4. [Local testing](docs/dev-setup.md?anchor=local-testing)